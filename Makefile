########################################################################
#                                                                      
#              --- CAEN SpA - Computing Division ---                   
#                                                                      
#   CAENVMElib Software Project                                        
#                                                                      
#   Created  :  Nov 2005      (Rel. 1.0)                                             
#                                                                      
#   Auth: NDA
#                                                                      
########################################################################

EXE	=	N957Run

CC	=	gcc

COPTS	=	-fPIC -DLINUX -Wall 
#COPTS	=	-g -fPIC -DLINUX -Wall 

FLAGS	=	-Wall -s
#FLAGS	=	-Wall

DEPLIBS	=       -lCAENN957 -lm

LIBS	=	

INCLUDEDIR =	-I. -I./include 

OBJS	=	main.o user_settings.o keyb.o

INCLUDES =	./include/N957Lib.h ./include/N957types.h ./include/N957oslib.h keyb.h user_settings.h

#########################################################################

all	:	$(EXE)

clean	:
		/bin/rm -f $(OBJS) $(EXE)

$(EXE)	:	$(OBJS)
		/bin/rm -f $(EXE)
		$(CC) $(FLAGS) -o $(EXE) $(OBJS) $(DEPLIBS)

$(OBJS)	:	$(INCLUDES) Makefile

%.o	:	%.c
		$(CC) $(COPTS) $(INCLUDEDIR) -c -o $@ $<

