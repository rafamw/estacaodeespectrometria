/*
        ----------------------------------------------------------------------

        --- CAEN SpA - Computing Systems Division ---

        a2818.h

        Header file for the CAEN N957 USB/NIM Bridge driver.

        April  2007 :   Created.

        ----------------------------------------------------------------------
*/
#ifndef _n957_H
#define _n957_H

#ifndef VERSION
	#define VERSION(ver,rel,seq) (((ver)<<16) | ((rel)<<8) | (seq))
#endif	


/*
        Defines for the n957
*/

#define N957_MAGIC                     'U'

#define N957_IOCTL_REV                 _IOWR(N957_MAGIC, 1, n957_rev_t)


/*
        ----------------------------------------------------------------------

        Types

        ----------------------------------------------------------------------
*/

// Rev 0.2
/*
	Struct for revision argument in ioctl calls
*/
#define N957_DRIVER_VERSION_LEN	20
typedef struct n957_rev_t {
        char 		rev_buf[N957_DRIVER_VERSION_LEN];
} n957_rev_t;


#endif
