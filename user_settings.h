/***********************************************************************************************
* MODULE:     user_settings.h
* PURPOSE:    user settng definitions
* WRITTEN BY: NDA
* COPYRIGHT:  CAEN S.p.A. all rights reserved
* USAGE:      compiled with gcc ?!?!
************************************************************************************************/
#ifndef __USER_SETTINGS_DEF_H
#define __USER_SETTINGS_DEF_H

////////////////////////////////////////////
// File includes
////////////////////////////////////////////
#include "common_defs.h"
#include "./include/N957oslib.h"
#include "./include/N957types.h"

////////////////////////////////////////////
// Global defines
////////////////////////////////////////////

#define MAX_FILENAME_LENGHT					255							/* maximum filename length */

typedef struct
{
	char* m_log_filename;					// output filename
	char* m_config_filename;				// Configuration filename
	char* m_gnu_plot_path;					// gnuplot path
	long m_max_num_samples;					// The maximum number of samples to acquire
	N957_UINT16 m_bldim;					// block dimension: maximum number of samples per call to retrive
	BOOL m_debug;						// debugging features enabled
	N957ControlModes m_mode;				// acqusition mode
	int32_t m_N957_handle;					// Board handle
	int m_bd_num;							// N957 board number
	N957_BYTE m_lld;						// LLD Threshold
	BOOL m_log_to_file;						// Write Data to output file
//	int m_gnu_plot_refresh;					// Gnu Plot refresh rate (msec)
//	float m_gnu_plot_x_scale;				// Gnu Plot X convertsion factor
} user_setting_data;
////////////////////////////////////////////
// Global variables declaration
////////////////////////////////////////////

////////////////////////////////////////////
// Global methods declaration
////////////////////////////////////////////


/***********************************************************************************************
* METHOD:     user_settings_open
* PURPOSE:    init user setting module
* PARAMETERS: <in> user_setting_data* p_data: data buffer pointer
* RETURN:     TRUE: module init ok
* USAGE:      call before any other methods
************************************************************************************************/
BOOL user_settings_open( user_setting_data* p_data);
/***********************************************************************************************
* METHOD:     user_settings_close
* PURPOSE:    closes user setting module (frees allocated resources)
* PARAMETERS: <in> user_setting_data* p_data: data buffer pointer
* RETURN:     TRUE: module init ok
* USAGE:      call before any other methods
************************************************************************************************/
BOOL user_settings_close( user_setting_data* p_data);
/***********************************************************************************************
* METHOD:     user_parse_input_param
* PURPOSE:    scan user input parameters
* PARAMETERS: <in> user_setting_data* p_data: data buffer pointer
*             <in> int argc: number of input parameters
*             <in> char* argv[]: string parameters list
* RETURN:     TRUE: input parameter parsing ok
* USAGE:      
************************************************************************************************/
BOOL user_settings_parse_input_param( user_setting_data* p_data, int argc, char* argv[]);






#endif 
