#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import os
import sys
import numpy as np
import time
import Tkinter as tk
import tkMessageBox
import tkFileDialog
import matplotlib.pyplot 

from matplotlib import pyplot, lines , animation
from time import gmtime, strftime
import pexpect
import subprocess
from User_Config import*
from Modulos import *
from Graficos import *
from pylab import *
#from clsbeep import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import Image, ImageTk



timer1=None


##############################################################################
##############################################################################
# GLOBAL VARIABLES AND DEFINITIONS

Control = dict(text_cursor='Cursor:',Refresh=300, bmin=0, bmax=512, bmaxold=512, j=0, filename='temp', count=0, paused=True, ROIctrl=list(),CALIctrl=list(),  Logaritmo=False, graphic1=True, graphic2=True, G_cursor=None, Grid=True, Aux_Cursor=None)		
FocusControl = dict( full_screen= False, aux_click='gg')
L_Graficos=list()
L_Modulos=list() #lista com módulos, instancias da classe  (arquivo Modulo)
ROIs=list()
Interface = dict(frames=list(), buttons=list(), labels=list(), spinboxes=list(), entries=list(), labelframes=list())

# Sai do programa
def leave():    
    global L_Modulos, sys
    if not  Control['paused']:
        pause()
    for mod in L_Modulos:
        mod.exit()
    sys.exit() 


##############################################################################
##############################################################################
# Main window

root = tk.Tk()
root.minsize(825,640)
root.title('Modulo Multicanal')
root.protocol('WM_DELETE_WINDOW', leave)



###################################################################
# Ve quantas variaveis MC_ estão criadas no arquivo User_Config.py
# Define o modo de aquisição:1, 2, 3 módulos (pode ser implementado para mais) 
try:
    MCA    # existe MCA no arquivo User_Config.py?
except NameError:
    tkMessageBox.showinfo("Erro", "MCA não encontrado, nenhum módulo selecionado!")
    sys.exit
else:
    L_Modulos.insert(0,Modulo('0'))  # cria uma instancia de "Modulo" e define umas variáveis iniciais
    L_Modulos[0].Nmr_serial= MCA
    L_Graficos.insert(0,Grafico(L_Modulos[0].Nmr_serial))
    L_Graficos[0].fig.suptitle('# A')
    L_Graficos[0].title=('A')


try:
    MCB     # existe MCB no arquivo User_Config.py?
except NameError:
    None
else:
    L_Modulos.insert(1,Modulo('1'))   # cria uma instancia de "Modulo"  e define umas variáveis iniciais
    L_Modulos[1].Nmr_serial= MCB
    L_Graficos.insert(1,Grafico(L_Modulos[1].Nmr_serial))
    L_Graficos[1].fig.suptitle('# B')
    L_Graficos[1].title=('B')

try: 
    MCC     # existe MCB no arquivo User_Config.py?  
except NameError:
    None
else:
    L_Modulos.insert(2,Modulo('2'))   # cria uma instancia de "Modulo"  e define umas variáveis iniciais
    L_Modulos[2].Nmr_serial=MCC
    L_Graficos.insert(2,Grafico(L_Modulos[2].Nmr_serial))
    L_Graficos[2].fig.suptitle('# C')
    L_Graficos[2].title=('C')    



sys.setrecursionlimit(5000) 
xx = arange(0, 8192)
xxx= arange(0,512)


##############################################################################
##############################################################################
# modifica um parametro da execução n957 (apartir do arquivo N957Run.confi)
def change_conf(parametro, valor):  # 
    arq=open('N957Run.conf','r+')
    arq_temp = list(arq)
    for line in range(len(arq_temp)):
         if any( parametro in  arq_temp[line]):
              arq_temp[line]= parametro +"            " + str(valor)+"\n" 
    arq.seek(0)
    for line in arq_temp:
        arq.write(line)    
    arq.close()




##############################################################################
##############################################################################
# Atualiza grade do plot de acordo com regiao escolhida(os "------")

def grid(subplot, n): # subplot = gráfico escolhido, n= numero de pontos no eixo x desse gráfico
    subplot.xaxis.set_major_locator(MultipleLocator(n/8)) # 
    if n > 64:
        subplot.xaxis.set_minor_locator(MultipleLocator(n/64))
        subplot.xaxis.grid(Control['Grid'],'major',linewidth=2)
    subplot.yaxis.grid(Control['Grid'],'major')
    subplot.xaxis.grid(Control['Grid'],'minor')

##############################################################################
##############################################################################
# Regiao de interesse 

# Destroy todos os ROIs previamente configurados
def Close_ROI():
    global roiwin
    for n in range(len(ROIs)):
        del ROIs[0]
    Control['ROIctrl'][:]=[]
    Control['j']=0
    roiwin.destroy()


def updateroi(fator):
    for n in range(len(ROIs)):
        ROIs[n].xmin = ROIs[n].xmin*fator/Control['bmaxold']
        ROIs[n].xmax = ROIs[n].xmax*fator/Control['bmaxold']
        ROIs[n].xmine.delete(0, 'end')
        ROIs[n].xmaxe.delete(0, 'end') 
        ROIs[n].xmine.insert(0, ROIs[n].xmin)
        ROIs[n].xmaxe.insert(0, ROIs[n].xmax)
    Control['bmaxold'] = fator

class InterestRegion:

    def __init__(self, labfrm, j):
        self.Area=list()

        for x in range(len(L_Modulos)):
            self.Area.insert(x, 0)
        self.text= ""
        for x in range(len(L_Modulos)):
            self.text=self.text + 'Conts#'+ str(x+1) +' '+ str(self.Area[x])  
            if not(x == 0):
                self.text=self.text +'\n'

        self.xmin = 0
        self.xmax = Control['bmax']-1
        self.xmine = tk.Entry(labfrm)
        self.xmine.pack(side='left')
        self.xmine.insert(0, self.xmin)
        self.label = tk.Label(labfrm, text='to')
        self.label.pack(side='left')
        self.xmaxe = tk.Entry(labfrm)
        self.xmaxe.pack(side='left')
        self.xmaxe.insert(0, self.xmax)        
        self.label2 = tk.Label(labfrm, text=self.text)
        self.label2.pack(side='bottom')

    def ROIme(self):
        global L_Modulos, L_Graficos
        if self.xmine.get().isdigit() and self.xmaxe.get().isdigit() and int(self.xmine.get()) >= 0 and int(self.xmaxe.get()) >= 0 and int(self.xmaxe.get()) < int(Interface['spinboxes'][0].get()) and int(self.xmine.get()) < int(Interface['spinboxes'][0].get()):
            for xx in range(len(L_Modulos)):
                if  int(self.xmaxe.get()) >= int(self.xmine.get()): 
                    self.xmin = int(self.xmine.get())
                    self.xmax = int(self.xmaxe.get())                
                elif int(self.xmaxe.get()) < int(self.xmine.get()):
                    self.xmin = int(self.xmaxe.get())
                    self.xmax = int(self.xmine.get())
            for x in range(len(L_Modulos)):
                self.Area[x] = 0

            if int(self.xmax) == int(self.xmin):
                for Mod in L_Modulos:
                    self.Area[L_Modulos.index(Mod)] = Mod.Data[int(self.xmax)]
            else:
                for n in range(self.xmin, self.xmax+1):
                    for Mod in L_Modulos:
                        self.Area[L_Modulos.index(Mod)] = self.Area[L_Modulos.index(Mod)] + Mod.Data[n]
            self.text=''    
            for xx in range(len(L_Modulos)):
                self.text=self.text + 'Conts#'+ str(xx+1) +' '+ str(self.Area[xx])
                if not(x == 0):
                    self.text=self.text +'\n'
            self.label2['text']=self.text            
        else:
            self.label2['text']="Invalid values"

def roiroi(): 
    global ROIs , roiwin
    Control['j'] = 0
    Labelsf = list()
    ROIs = list()
    roiwin = tk.Tk()
    roiwin.title('ROI')

    def addROI():
        Labelsf.insert(Control['j'], tk.LabelFrame(roiwin, text='ROI %d' %Control['j']))
        Labelsf[Control['j']].pack(side='top')
        ROIs.insert(Control['j'], InterestRegion(Labelsf[Control['j']], Control['j']))
        Control['ROIctrl'].insert(Control['j'], 0)
        Control['ROIctrl'][int(Control['j'])] = 1
        ROIs[Control['j']].ROIme()
        Control['j'] = Control['j'] + 1

    roibut = tk.Button(roiwin, text = 'Add', command = lambda i=i: addROI(), bd=2, width=7, height=1)
    roibut.pack(side='top')
    addROI()
    roiwin.protocol('WM_DELETE_WINDOW', Close_ROI)
    roiwin.mainloop()




def upd_CursorLabel():
    Control['text_cursor']='Cursor '
    for G in L_Graficos: # calcula o texto lá embaixo que indica a contagem no canal do cursor. 
        Control['text_cursor']= Control['text_cursor'] + G.title + ':(' +str(G.Cursor['x']) + ','+str(L_Modulos[L_Graficos.index(G)].Data[G.Cursor['x']])+ ') '  
        G.fig.canvas.draw()
    Interface['labels'][1+3*len(L_Graficos)].configure(text= Control['text_cursor'])

   

##############################################################################
##############################################################################

def set_fator(canais): # Define o fator para conversao do numero de canais
    global Control , L_Modulos
    for Mod in L_Modulos:
        Mod.set_fator(canais) #manda os módulos mudar o nmr de canais recebidos pelo programa em C. 

    for G in L_Graficos: # refaz o cursor, para ficar visualmente no mesmo lugar
        G.draw_Cursor(G.Cursor['x']*int(canais)/int(Control['bmax']))
        G.limupdty = True
        G.limupdtx = True
        G.limtrans = False
        G.subplot.set_xlim(xmin=0,xmax=int(Interface['spinboxes'][0].get()))

    for Mod in L_Modulos:
        Mod.dump()

    graphic()

    upd_CursorLabel()

    Control['bmax']= int(canais)
    updateroi(int(canais))

     
def testeerro(Data):   # mensagens de erros
    if not Data == 'OK':
        if  Data == 'error_1':
            print ("erro_1")            
            tkMessageBox.showinfo("Erro", "user_settings_open erro! ")
            return False	
        elif  Data == 'erro_2': # Módulo não esta ligado, não esta rodando como root
            print ('Módulo não esta ligado ou não esta rodando como root')
            tkMessageBox.showinfo("Erro", "user_settings_parse_input_param error!")
            return False	
        elif  Data == 'erro_3':
            print ('erro_3')
            tkMessageBox.showinfo("Erro", "N957_GetFWRelease error!")
            return False
        elif  Data == 'erro_4':
            print ('erro_4')
            tkMessageBox.showinfo("Erro", "Problemas ao iniciar")
            return False
        elif  Data == 'erro_5':
            print ('erro_5')
            tkMessageBox.showinfo("Erro", "N957_GetScaler")
            return False
        elif  Data == 'erro_6':
            print ('erro_6')
            tkMessageBox.showinfo("Erro", "N957_StartAcquire")
            return False
        elif  Data == 'erro_7':
            print ('erro_7')
            tkMessageBox.showinfo("Erro", "N957_SetSwConvFlag")
            return False
        elif  Data == 'erro_8':
            tkMessageBox.showinfo("Erro", "Erro ao  alocar memória ")
            return False
        elif  Data == 'erro_9':
            tkMessageBox.showinfo("Erro", "N957_ReadData")
            return False
        else:
            tkMessageBox.showinfo("Erro_default", Data)
            return False
    else:
        return True



##############################################################################
##############################################################################
#  as 2 principais funçoes  graphic e graphic2

def graphic2(): #trata as informações que foram salvas em graphic1
    
    #print time.time()-timer1
    for G in L_Graficos:

        #if Control['Logaritmo']:
        #     G.Data[0].set_data(arange(int(L_Modulos[L_Graficos.index(G)].Controle['Nmr_canais'])),log(L_Modulos[L_Graficos.index(G)].Data))
        #else:    
        G.Data[0].set_data(arange(int(L_Modulos[L_Graficos.index(G)].Controle['Nmr_canais'])),L_Modulos[L_Graficos.index(G)].Data) #Atualiza o range do plot


    print G.limupdtx , G.limupdty
    for G in L_Graficos:
        G.subplot.relim()   
        if G.limupdtx and G.limupdty:
            G.subplot.autoscale(enable=True, axis='both')
        else:
            if G.limupdty:
                G.subplot.autoscale(enable=True, axis='y')
            else:
                G.subplot.autoscale(enable=True, axis='x')
        G.subplot.autoscale_view(scaley=G.limupdty,scalex=G.limupdtx)
            
    
    for G in L_Graficos:         
       if G.limupdtx == True:
            grid(G.subplot, int(L_Modulos[L_Graficos.index(G)].Controle['Nmr_canais']))
      

    upd_CursorLabel()

def graphic(): # Le os dados provenientes dos módulos 
    global timer1
    timer1= time.time()
    for Mod in L_Modulos:
        testeerro(Mod.Read_Data())  #LE OS DADOS

    aux=0;
    for Mod in L_Modulos:

        Interface['labels'][aux].configure(text='ADC : %s' %str(Mod.Livetime['ADC_Conversion']))
        Interface['labels'][aux+1].configure(text='Total Time: %ss' %str(Mod.Livetime['Time']/1000))
        Interface['labels'][aux+2].configure(text='Dead Time: %.5f %%' %(Mod.Livetime['DeadTime']))
        aux=aux+3 #CUIDADO para implementar + de 2 módulos  Interface['labels'] deve ser re-organizado
    
    graphic2()

    
   
    #print time.time()-timer1
   
    
def realtimeplot(): # Funcao recursiva para plot em tempo real
    if not Control['paused']:
        for Mod in L_Modulos:
            Mod.dump()
        try:
            graphic()
            root.after(Control['Refresh'], realtimeplot)
            for n in range(len(Control['ROIctrl'])):
                if Control['ROIctrl'][n] == 1:
                    ROIs[n].ROIme()
        except ValueError: 
            pass



##############################################################################
##############################################################################
# Control functions 

def  Hide_Unhide():
    global L_Graficos
    aux=0 #var usada para contar o numero de gráficos que devem aparecer
    aux2=0 # var usada na hora da varredura, para fazer o plot

    for G in L_Graficos:
        if G.Checkbutton_Plot.get() == 1:
            aux = aux +1

    if aux > 0:
        for G in L_Graficos:
            G.Frame.place_forget()
        for G in L_Graficos:
            if G.Checkbutton_Plot.get() ==1:     
                G.Frame.place(anchor='nw',relheight=1, relwidth= 1./aux, relx = 1.*(L_Graficos.index(G)-aux2)/aux)
            else:
                aux2 = aux2 +1

def reset(): # Zera o vetor de aquisicao
    Shift_Cursor(True,0)
    for Mod in L_Modulos:
        Mod.reset()     
    graphic2()

#LOG
def Change_Log_Scale():
    if Control['Logaritmo']:
        Control['Logaritmo']= False
        for G in L_Graficos:
            G.fig.gca().set_yscale('linear')
        Control['Refresh']=700
        #tkMessageBox.showinfo("Attention", "Refresh time was set to 0.7 seconds")
    else:
        Control['Logaritmo'] = True    
        for G in L_Graficos:
            G.fig.gca().set_yscale('log')
        Control['Refresh']=700
        tkMessageBox.showinfo("Attention", "Refresh time was set to 1.5 seconds")
        #for G in L_Graficos:
        #     m,M= G.subplot.get_ylim()
        #     G.subplot.set_ybound(upper=log(M))
    
    

def Change_Grid():
    if Control['Grid']:
        Control['Grid']= False
    else:
        Control['Grid'] = True
    
    for G in L_Graficos:         
        grid(G.subplot, int(L_Modulos[L_Graficos.index(G)].Controle['Nmr_canais']))




def pause(): # Para/Continua a aquisicao de dados

    Control['paused'] = not Control['paused']
    for Mod in L_Modulos:
        Mod.pause()
            
    if Control['paused']:
        Interface['buttons'][1]['text'] = 'Resume'
        Interface['labels'][3*len(L_Graficos)]['text'] = 'Paused' 
    else:
        Interface['buttons'][1]['text'] = 'Pause'
        Interface['labels'][3*len(L_Graficos)]['text'] = 'Acquiring' 
        realtimeplot()
  
def save(): # Salva os dados no arquivo selecionado (grafico= g1(salva g1) g2(salva g2) g12(salva os dois)
    global  Data, L_Modulos
    
    Interface['labels'][3*len(L_Graficos) +2].configure(text='%s.%d' %(Control['filename'], Control['count'])) #atualiza label da direita e embaixo
    Control['count'] = Control['count'] + 1
    for Mod in L_Modulos: 
        fname = open(Control['filename'] +'_'+str(Control['count'])+'_MC'+ L_Graficos[L_Modulos.index(Mod)].title+'.dat' , mode='w')
        
        for n in range(int(Interface['spinboxes'][0].get())):
            fname.write(str(n+1)+' '+str(Mod.Data[n])+'\n')
        fname.close()

    fname = open(Control['filename'] +'_'+str(Control['count'])+'.log' , mode='w')
    fname.write(strftime("%a, %d %b %Y %X \n\n", gmtime()))
    for Mod in L_Modulos:
        fname.write("MC"+ L_Graficos[L_Modulos.index(Mod)].title +":\n")
        fname.write("LiveTime: "+ str(Mod.Livetime['Time'])+'\n')
        fname.write("DeadTime: "+ str(Mod.Livetime['DeadTime'])+'%\n')
        fname.write("ADC_Conversion:  " + str(Mod.Livetime['ADC_Conversion'])+'\n')
        fname.write("Serial Number:   " + str(Mod.Controle['Nmr_serial'])+'\n\n')
    fname.close()

   


    Interface['labels'][3*len(L_Graficos) +2]['text'] = '%s.%d' %(Control['filename'], Control['count'])

def load_buff():
    
    for Mod in L_Modulos:
        fname = open('Buffer.MC'+ L_Graficos[L_Modulos.index(Mod)].title , mode='w')        
        for n in range(int(Interface['spinboxes'][0].get())):
            fname.write(str(n+1)+' '+str(Mod.Data[n])+'\n')
        fname.close()
    subprocess.Popen([sys.executable, "buffer.py"])
    
def saveas(): # Salva os dados no arquivo selecionado

    temp = tkFileDialog.asksaveasfilename(title='Save as')
    
    if temp != str():        
        Control['filename'] = temp
        Control['count']=0

    for Mod in L_Modulos:
        fname = open(Control['filename'] +'_'+str(Control['count'])+'_MC'+ L_Graficos[L_Modulos.index(Mod)].title+'.dat' , mode='w')
        #fname = open(Control['filename'] +'_'+str(Control['count'])+L_Graficos[L_Modulos.index(Mod)].title , mode='w')
        
        for n in range(int(Interface['spinboxes'][0].get())):
            fname.write(str(n+1)+' '+str(Mod.Data[n])+'\n')
        fname.close()

    fname = open(Control['filename'] +'_'+str(Control['count'])+'.log' , mode='w')
    fname.write(strftime("%a, %d %b %Y %X \n\n", gmtime()))
    for Mod in L_Modulos:
        fname.write("MC"+ L_Graficos[L_Modulos.index(Mod)].title +":\n")
        fname.write("LiveTime: "+ str(Mod.Livetime['Time'])+'\n')
        fname.write("DeadTime: "+ str(Mod.Livetime['DeadTime'])+'%\n')
        fname.write("ADC_Conversion:  " + str(Mod.Livetime['ADC_Conversion'])+'\n')
        fname.write("Serial Number:   " + str(Mod.Controle['Nmr_serial'])+'\n\n')
    fname.close()

 
    Interface['labels'][3*len(L_Graficos) +2]['text'] = '%s.%d' %(Control['filename'], Control['count'])
    Control['count'] = Control['count'] + 1
    Interface['labels'][3*len(L_Graficos) +2].configure(text='%s.%d' %(Control['filename'], Control['count']))
 
def load(): # Manda os dados carregados para leitura

    newfile = tkFileDialog.askopenfilename()
    filefile = np.loadtxt(newfile)
    tempfile = open('save.dat','w+')
    set_fator(len(filefile))
    for h in filefile[:,1]:
        tempfile.write(str(int(h))+'\n')
    tempfile.close()
    for Mod in L_Modulos:
        Mod.load()
        time.sleep(0.1) 


def refreshrate(): # Define a frequencia de atualizacao do grafico
    rrwin = tk.Tk()
    rrwin.title('Set Refresh Rate')
    rrent = tk.Entry(rrwin)
    rrent.pack(side='left')
    rrlab = tk.Label(rrwin, text='ms')
    rrlab.pack(side='left')
    
    def setrr():
        if rrent.get().isdigit():
            if int(rrent.get()) > 50: # Valores muito baixos causam stack overflow
                Control['Refresh'] = int(rrent.get())
                #Interface['labels'][4].configure(text = '     Refresh Rate %dms' %Control['Refresh'])
                rrwin.destroy()
            else:
                tkMessageBox.showinfo("Warning", "This refresh rate is likely to reach maximum recursion depht!")
                Control['Refresh'] = 99
        else:
            tkMessageBox.showinfo("Error", "Invalid value")

    rrbut = tk.Button(rrwin, text = 'Set', command = lambda i=i: setrr(), bd=2, width=7, height=1)
    rrbut.pack(side='left')
    rrwin.mainloop()

def str_cali(): #definir a reta de calibração ainda não implementado
    caliwin = tk.Tk()
    caliwin.title('Straight Calibration')
    calient = tk.Entry(caliwin)
    calient.pack(side='right')
    Control['j'] = 0

    def addpoint():
        Labelsf.insert(Control['j'], tk.LabelFrame(caliwin, text='Point %d' %Control['j']))
        Labelsf[Control['j']].pack(side='top')
        #ROIs.insert(Control['j'], InterestRegion(Labelsf[Control['j']], Control['j']))
        Control['CALIctrl'].insert(Control['j'], 0)
        Control['CALIctrl'][int(Control['j'])] = 1
        Control['j'] = Control['j'] + 1

    def least_squares(x,y):
        A = np.vstack([x, np.ones(len(x))]).T
        a, b = np.linalg.lstsq(A, y)[0]
        return a ,b

    calibut = tk.Button(caliwin, text = 'Add', command = lambda i=i: addpoint(), bd=2, width=7, height=1)
    calibut.pack(side='top')
    addpoint()
    caliwin.mainloop()
	

    

def lldt():  #Definir o lower level detection trueshot
    lldtwin = tk.Tk()
    lldtwin.title('Low level Detect Treshold')
    lldtent = tk.Entry(lldtwin)
    lldtent.pack(side='left')
    lldtlab = tk.Label(lldtwin, text='%')
    lldtlab.pack(side='left')
    
    def setlldt():

        if lldtent.get().isdigit():
            if int(lldtent.get()) < 100: # Percentual
                Interface['buttons'][1].invoke()
                for Mod in L_Modulos:
                    Mod.LLDT(lldtent.get())              
                lldtwin.destroy()
                tkMessageBox.showinfo("Cuidado!", "Execução interrompida!")
        else:
            tkMessageBox.showinfo("Error", "Invalid value: Percent based")

    lldtbut = tk.Button(lldtwin, text = 'Set', command = lambda i=i: setlldt(), bd=2, width=7, height=1)
    lldtbut.pack(side='left')
    lldtwin.mainloop()

def Reset_Plot():
    G=G_cursor
    G.limupdty = True
    G.limupdtx = True
    G.limtrans = False
    G.subplot.set_xlim(xmin=0,xmax=int(Interface['spinboxes'][0].get()))

def Open_About():
    AboutWin= tk.Toplevel()
    #http://stackoverflow.com/questions/20251161/tkinter-tclerror-image-pyimage3-doesnt-exist - LINK SALVADOR  
    AboutWin.resizable(width=False, height=False)
    
    AboutWin.configure(bg='white')
    AboutWin.title('About')
    tk.Label(AboutWin, text="Authors:   Julio Cesar Ferreira Tambara ",bg='white' ).pack(side='top',fill='x')      
    tk.Label(AboutWin, text="       Mateus Vicente Wrasse Wiebusch Müller ",bg='white').pack(side='top',fill='x')
    tk.Label(AboutWin, text="Advisor: Rafael Pereti Pezzi ",bg='white').pack(side='top',fill='x')

    imagens = tk.Frame(AboutWin,takefocus=0,bg='white')
    Imagem_load =os.path.dirname(os.path.realpath(__file__))+ "/Imagens"

    photo = ImageTk.PhotoImage(file=Imagem_load + '/Lab_cta.png')
    #photo2= ImageTk.PhotoImage(file=Imagem_load +'/implantador130.png' ) 
 
    tk.Label(imagens, image=photo,bg='white').pack(side='left')
    #tk.Label(imagens, image=photo2,bg='white').pack(side='left')
    imagens.pack(side='top',fill='both')

    tk.Label(AboutWin, text="Repository: http://cta.if.ufrgs.br/projects/estacao-de-espectrometria/repository ",bg='white').pack(side='top',fill='x')
    tk.Label(AboutWin, text="Laboratory: http://implantador.if.ufrgs.br ",bg='white').pack(side='top',fill='x')
    tk.Label(AboutWin, text="License: GNU General Public License V3 ",bg='white').pack(side='top',fill='x')
    tk.Label(AboutWin, text="Contact:julio.tambara@ufrgs.br",bg='white').pack(side='top',fill='x')
   
    AboutWin.mainloop()


    
 


##############################################################################
##############################################################################
# Interface elements

for i in range(4): #cria 4 frame
    if i == 2:
        Interface['frames'].insert(i, tk.Frame(root,takefocus=0,bg='grey'))
        Interface['frames'][i].pack(side='top',fill='both', expand=1)
    else:
        Interface['frames'].insert(i, tk.Frame(root))
        Interface['frames'][i].pack(side='top',fill='x', expand=0)

Frame_bin_scale=tk.Frame(Interface['frames'][0],takefocus=0,bg='grey')
Frame_bin_scale.pack(side='right')

Interface['labelframes'].insert(0,tk.LabelFrame(Frame_bin_scale,relief='raised',takefocus=0, text='Bins', bd=1, pady=0, bg='grey',height=2))
Interface['labelframes'].insert(1,tk.LabelFrame(Interface['frames'][0], relief='raised',takefocus=0, text='Time', bd=1, height=5, pady=2, bg='grey'))
Interface['labelframes'].insert(2,tk.LabelFrame(Frame_bin_scale, relief='raised',takefocus=0, bd=2, pady=2, bg='grey',height=2))

Interface['labelframes'][1].pack(side='right',fill='both')

Interface['labelframes'][0].pack(side='top')  #frame dos botoes
Interface['labelframes'][2].pack(side='top',fill='x')

for i in range(3):
    Interface['frames'].insert((i+4), tk.Frame(Interface['labelframes'][0]))
    Interface['frames'][(i+4)].pack(side='left')

var = tk.StringVar(Interface['labelframes'][0])

Interface['spinboxes'].insert(0, tk.Spinbox(Interface['labelframes'][0], command = lambda i=i: set_fator(Interface['spinboxes'][0].get()), values=[128,256,512,1024,2048,4096,8192],textvariable =var,takefocus=0, width = 9, state = 'readonly', bg='grey'))
var.set("512")
Interface['spinboxes'][0].pack()

tk.Checkbutton(Interface['labelframes'][2], text="Log Scale",takefocus=0,command=lambda i=i:Change_Log_Scale(), variable = Control['Logaritmo'],bg='grey').pack(expand=1,fill='x')
Checkbutton_grid = tk.Checkbutton(Interface['labelframes'][2], text="Grid",takefocus=0,command=lambda i=i:Change_Grid(), bg='grey')
Checkbutton_grid.select()
Checkbutton_grid.pack(expand=1,fill='x')



##############################################################################
# Menu
menubar = tk.Menu(root, relief='raised', bd=2)
filemenu = tk.Menu(menubar, tearoff=0)
filemenu.add_command(label="Load Data", command = lambda i=i:load())
filemenu.add_command(label ="Save", command = lambda i=i: save())
filemenu.add_command(label ="Save As", command = lambda i=i: saveas())
filemenu.add_command(label="Exit", command = lambda i=i:leave())

settings = tk.Menu(menubar, tearoff=0)
settings.add_command(label="Low level detect treshold", command = lambda i=i:lldt())
settings.add_command(label="Plot Refresh Rate", command = lambda i=i:refreshrate())
settings.add_command(label="Regions of Interest", command = lambda i=i: roiroi())
#settings.add_command(label="Straight Calibration", command = lambda i=i:str_cali())

buffermenu = tk.Menu(menubar, tearoff=0)
buffermenu.add_command(label="Load", command = lambda i=i:load_buff())


rig_but_menu= tk.Menu(root, tearoff=0)
rig_but_menu.add_command(label="Reset Plot", command = lambda i=i:Reset_Plot())
rig_but_menu.add_command(label="Set Cursor", command = lambda i=i:G_cursor.draw_Cursor(Control['Aux_Cursor']))
#rig_but_menu.add_command(label="Change_plot", command = lambda i=i:load())
#rig_but_menu.add_command(label="Hide graphic", command = lambda i=i:hide_graphic())
#rig_but_menu.add_command(label="Select graphic", command = lambda i=i:L_Graficos[1].Canvas.get_tk_widget().focus_set())


helpmenu=tk.Menu(root,tearoff=0)
helpmenu.add_command(label="About", command= lambda i=i: Open_About())
helpmenu.add_command(label="Mouse Commands", command= lambda i=i: Open_About())


#menubar.add_cascade(label="Reset Zoom", menu=reset_zoom)
menubar.add_cascade(label="File", menu=filemenu)
menubar.add_cascade(label="Settings", menu=settings)
menubar.add_cascade(label="Buffer", menu=buffermenu)
menubar.add_cascade(label="Help", menu= helpmenu)

root.config(menu=menubar)
##############################################################################
Frame_Buttons=tk.Frame(Interface['frames'][0],takefocus=0,bg='grey')

Interface['buttons'].insert(0, tk.Button(Frame_Buttons, takefocus=0,  text = 'Start', command = lambda i=i: realtimeplot(), bd=2,  height=2, bg='grey'))
Interface['buttons'].insert(1, tk.Button(Frame_Buttons,takefocus=0,  text = 'Start', command = lambda i=i: pause(), bd=2,  height=2, bg='grey'))
Interface['buttons'].insert(2, tk.Button(Frame_Buttons,takefocus=0,  text = 'Clear', command = lambda i=i: reset(), bd=2,  height=2, bg='grey'))
Interface['buttons'].insert(3, tk.Button(Frame_Buttons,takefocus=0,  text = 'Save', command = lambda i=i: save(), bd=2,  height=2, bg='grey'))
Interface['buttons'].insert(4, tk.Button(Frame_Buttons,takefocus=0,  text = 'Save As', command = lambda i=i: saveas(), bd=2, height=2, bg='grey'))


Frame_checkbuttons=tk.Frame(Interface['frames'][0],takefocus=0,bg='grey')# frame com os ckeckbuttons dos plots


for G in L_Graficos:    
    aux=tk.Checkbutton(Frame_checkbuttons, text=G.title, variable=G.Checkbutton_Plot, width=5, takefocus=0,command=lambda i=i:Hide_Unhide(), bg='grey')
    aux.select()
    aux.pack(side='left',fill='x')

Frame_checkbuttons.pack(side='bottom',fill='x')
Frame_Buttons.pack(side='top',fill='both',expand=1)

for i in range(4):
    Interface['buttons'][i+1].pack(side = 'left', fill='both', expand=1)




for i in range(3):
    Interface['frames'].insert(i+7, tk.Frame(Interface['frames'][3]))
    Interface['frames'][i+7].pack(side = 'left', fill='x', expand=1)

for G in L_Graficos:
   G.Frame= tk.Frame(Interface['frames'][2])
   G.Frame.place(anchor='nw',relheight=1, relwidth= 1./len(L_Graficos), relx = 1.*(L_Graficos.index(G))/len(L_Graficos))

Interface['frames'].insert(10, None)#arrumar depois
Interface['frames'].insert(11, None)

for x in range(len(L_Modulos)):
    Interface['frames'].insert(12 + x, tk.Frame(Interface['labelframes'][1],bg='grey'))

    

aux=0
for Mod in L_Modulos:
    Interface['labels'].insert(aux,tk.Label(Interface['frames'][12+aux/3], text='ADC: %s' %Mod.Livetime['ADC_Conversion'], width=13, height=1, bg='grey'))
    Interface['labels'][aux].pack(side='left')

    Interface['labels'].insert(aux+1,tk.Label(Interface['frames'][12+aux/3], text='Total Time: %s' %Mod.Livetime['Time'], width=18, height=1, bg='grey'))
    Interface['labels'][aux+1].pack(side='left')

    Interface['labels'].insert(aux+2,tk.Label(Interface['frames'][12+aux/3], text='Dead Time: %.5f ' %Mod.Livetime['DeadTime'], width=18, height=1, bg='grey'))
    Interface['labels'][aux+2].pack(side='left')

    aux=aux+3 

for x in range(len(L_Modulos)):
    Interface['frames'][12+x].pack(side='top',fill='both', expand='y')

Interface['labels'].insert(aux,tk.Label(Interface['frames'][7], text='Waiting to Start', anchor='w'))
Interface['labels'][aux].pack(fill='x')

Interface['labels'].insert(aux+1,tk.Label(Interface['frames'][8], text= Control['text_cursor']  , anchor='w'))
Interface['labels'][aux+1].pack(side='left')
Interface['labels'].insert(aux+2,tk.Label(Interface['frames'][9], text=os.path.dirname(os.path.realpath(__file__))+'/Save/%s.%d' %(Control['filename'], Control['count']), anchor='e'))
Interface['labels'][aux+2].pack(fill='x')
Interface['labels'].insert(aux+3,tk.Label(Interface['frames'][8], text= '(0,0)' , anchor='w'))
Interface['labels'][aux+3].pack(side='right')



##############################################################################
##############################################################################
# Key Bindings


#def key_1():
#    Interface['buttons'][1].invoke()
#def key_2():
#    reset()
#def key_3():
#    roiroi()
def key_4(event): #'button_press_event'
    G=G_cursor
    if event.inaxes != G.subplot.axes:   # se não estiver dentro dos eixos, apenas pega o foco
        G.Canvas.get_tk_widget().focus_set()
        return
    G.pressx = float(event.xdata)
    G.pressy = float(event.ydata)

    if event.button == 3:#botao direito do mouse
        G.limupdtx = G.limupdtx 


  
    elif event.button == 2:#botao do meio do mouse
        G.limupdty = True
        G.limupdtx = True
        G.limtrans = False
        G.subplot.set_xlim(xmin=0,xmax=int(Interface['spinboxes'][0].get()))

    elif event.button == 1:#esq
        G.limupdtx = False
        G.limtrans = True

def key_5(event):# 'motion_notify_event'
    G=G_cursor
    if event.inaxes != G.subplot.axes: return
    Interface['labels'][3+3*len(L_Graficos)].configure(text='  (%.1f' %float(event.xdata)+ ',%.1f)'%float(event.ydata))




def key_6(event): #'button_release_event'
   
    G=G_cursor
    Control['Aux_Cursor']=int(event.xdata)
    Xm , XM = G.subplot.get_xlim()
    Ym , YM = G.subplot.get_ylim()
   

    #if event.inaxes != G.subplot.axes:
    if event.button == 3:
        FocusControl['aux_click']='g1'
        xp,yp= G.fig.canvas._tkcanvas.winfo_pointerxy()      
        rig_but_menu.tk_popup(xp,yp)
  
    if event.button == 1:
        
        #print  event.y, event.x

   
        #print G.Canvas._tkcanvas.winfo_pointerxy()
        #print G.Frame.winfo_pointerxy()
        #print root.winfo_pointerxy()
        #print G.fig.canvas.get_tk_widget()
        #print G.Canvas.canvasx(10)
        #print G.Canvas.canvasy(0)




     
        if event.ydata==None:  #testa para ver se a pessoa nao clicou fora do eixo Y
            print "Y=", event.y
            if event.y >450:   #testa para saber se clicou em cima do gráfico ou embaixo do gráfico
                Yrelease= YM
            else: 
                Yrelease= Ym
        else:
            Yrelease=event.ydata
        
        if event.xdata==None:   #testa para ver se a pessoa nao clicou fora do eixo X
            print "X=",event.x
            if event.x>450:      #testa para saber se clicou a direita do gráfico ou a esquerda
                Xrelease=XM
            else:
                Xrelease=Xm
        else:
            Xrelease=event.xdata
          

        if G.limtrans == True:
            if G.pressx >= Xrelease:
                xx = Xrelease
                xx2 = G.pressx
            else:
                xx2 = Xrelease
                xx = G.pressx
            if G.pressy >= Yrelease:    
                yy = Yrelease
                yy2 = G.pressy
            else:
                yy2 = Yrelease
                yy = G.pressy
            if abs((xx2-xx)/(XM-Xm)) >= abs((yy2-yy)/(YM-Ym)) and (xx2-xx) > 0.03*(XM-Xm): 
               # print xx2, xx , XM, Xm
               # print yy2, yy, YM, Ym
              
                G.subplot.set_xlim(xx,xx2)
                G.limupdtx = False
            elif (yy2-yy) > 0.03*(YM-Ym): 
                G.subplot.set_ylim(yy,yy2, auto=True)
                G.limupdty = False
        G.limtrans = False

    if int(XM-Xm)>8:
        grid(G.subplot, int(XM-Xm))
        G.fig.canvas.draw()
    else:
        grid(G.subplot, 8)
        G.fig.canvas.draw()






def get_x(event):
    global G_cursor
    for a in range(len(L_Graficos)):
        if event.x_root-root.winfo_rootx() <= float((root.winfo_width())*(a+1)/len(L_Graficos)):
            G_cursor=L_Graficos[a]
            return       
    
                

root.bind_all("<Alt-KeyPress-1>", lambda i=i:Interface['buttons'][1].invoke())
root.bind_all("<Alt-KeyPress-2>", lambda i=i:Interface['buttons'][1].invoke())
root.bind_all("<Alt-KeyPress-3>", lambda i=i:reset())
root.bind_all("<Alt-KeyPress-6>", lambda i=i:roiroi())
root.bind_all("<Alt-KeyPress-4>", lambda i=i:save())
root.bind_all("<Alt-KeyPress-5>", lambda i=i:saveas())


def Shift_Cursor(all_grafic, nmr):  
    global Control , L_Modulos
    for G in L_Graficos:
        if ((G.Frame.focus_get() == G.Canvas.get_tk_widget()) or (all_grafic==1)) :
            if not (G.Cursor['x']>= ((Control['bmax']-1)+nmr) or G.Cursor['x']<=1+nmr): 
                G.draw_Cursor(G.Cursor['x']+nmr)                      
    upd_CursorLabel()
   

def teste():
   global L_Graficos
   print 'hi'
   PLOT22()
   for G in L_Graficos: 
       G.fig.canvas.show()       



root.bind_all("<Up>", lambda i=i:add_cursor())
root.bind_all("<T>", lambda i=i:teste())
root.bind_all("<Shift-Up>",lambda i=i:mult_zoom(1.25,1))
root.bind_all("<Down>",lambda i=i:mult_zoom(0.8,0))
root.bind_all("<Shift-Down>",lambda i=i: mult_zoom(0.8,1))

root.bind_all("<Left>",lambda i=i: Shift_Cursor(False,-1))
root.bind_all("<Shift-Left>",lambda i=i: Shift_Cursor(True,-1))
root.bind_all("<Right>",lambda i=i: Shift_Cursor(False,1))
root.bind_all("<Shift-Right>",lambda i=i: Shift_Cursor(True,1))





##############################################################################
##############################################################################
# Plotting embedded #PLOT!

for G in L_Graficos:
    G.Canvas= FigureCanvasTkAgg(G.fig, master=G.Frame)
    G.Canvas.get_tk_widget().pack(side='top', fill='both', expand=1)
    G.Data = G.subplot.plot(xxx, xxx*0,'o',color='yellow', lw=0.9)#,animated=True)
    G.subplot.set_xlabel('Channel')
    G.subplot.set_ylabel('Counts')

xlabel('Channel')
ylabel('Counts')

for G in L_Graficos:
    G.fig.canvas.mpl_connect('button_press_event', key_4)
    G.fig.canvas.mpl_connect('motion_notify_event', key_5)
    G.fig.canvas.mpl_connect('button_release_event', key_6)
    G.Frame.bind("<Enter>", get_x)

##############################################################################
##############################################################################
#Start
#Interface['labels'][3*len(L_Modulos)].configure(text="Connecting to #%d" %Mod.Nmr_serial)
#print os.path.dirname(os.path.realpath(__file__))
#def ANIMACAO(i):
#    for G in L_Graficos:
#        G.Data.set_xdata(range(int(L_Modulos[L_L_Graficoss.index(G)].Controle['Nmr_canais']))) #Atualiza o range do plot
#        G.Data.set_ydata(L_Modulos[L_Graficos.index(G)].Data)

   
#anim = animation.FuncAnimation(L_Graficos[0].f, ANIMACAO)


temp = tkFileDialog.asksaveasfilename(title='File Name',initialdir= os.path.dirname(os.path.realpath(__file__))+'/Save')  
Control['filename'] = temp
Control['count']=0
Interface['labels'][3*len(L_Graficos) +2]['text'] = '%s.%d' %(Control['filename'], Control['count'])
Interface['labels'][3*len(L_Graficos) +2].configure(text='%s.%d' %(Control['filename'], Control['count']))
#sys.getfilesystemencoding() 




for Mod in L_Modulos:
    #Interface['labels'][3*len(L_Modulos)]['text']= "Connecting to #%d", %Mod.Nmr_serial
    #while not testeerro(Mod.start()):
        #if not tkMessageBox.askretrycancel("Módulo não Encontrado", "Verifique se os todos os módulos estão ligados! "):
            #sys.exit
        #else:
            #sys.exit
    
    if not  testeerro(Mod.start()):
        leave()    # mostra uma msg de erro e sai do programa.
for Mod in L_Modulos:
    Mod.reset()
    










#canvas.mpl_connect('button_press_event', tab)
##############################################################################
##############################################################################
# Paralel mainloop and initiations

#for n in range(2):
#    time.sleep(0.5)
#    Interface['spinboxes'][0].invoke('buttonup')



root.mainloop()

##############################################################################
##############################################################################

