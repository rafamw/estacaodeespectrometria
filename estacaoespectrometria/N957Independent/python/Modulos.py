import Tkinter as tk
import pexpect
import time
import numpy as np
import random
from array import *

##############################################################################
##############################################################################
#Classe que faz o Pipe com o programa em C

class Modulo:

    def __init__(self, Nmr_modulo):
         #self.child = pexpect.spawn('./N957Run '+ Nmr_modulo)    
         #self.Controle['Nmr_serial']=Serial 
         self.Data=array('L',[0]*512)
         self.Controle=dict(pause=1,Nmr_serial=0,Nmr_canais=512, ON=0)
         self.Livetime = dict(ADC_Conversion=0 , Time=0. , DeadTime=0.)

    def start(self):
         return 'OK'
	#self.child.send('s')
    #    msg=(self.child.readline().strip("\r\n"))

    #    return msg


    def set_fator(self, canais):  # Define o fator para conversao do numero de canais

        self.Controle['Nmr_canais']=canais
        #if int(canais) == 8192:   # 8192 pontos
#            self.child.send('a')   
#        elif int(canais) == 4096: # 4096 pontos
#            self.child.send('b') 
#        elif int(canais) == 2048: # 2048 pontos
#            self.child.send('c')
#        elif int(canais) == 1024: # 1024 pontos
#            self.child.send('d')
#        elif int(canais) == 512:  # 512 pontos
#            self.child.send('e')
#        elif int(canais) == 256:  # 256 pontos
#            self.child.send('f')
#        elif int(canais) == 128:  # 128 pontos
#            self.child.send('g')
#        elif int(canais) == 64:   # 64 pontos
#            self.child.send('h')

    def load(self): 
        pass
        #self.child.send('y')

    def LLDT(self, lldt):
        pass
        #self.child.send('j')
        #self.child.send(chr(int(lldt)/10))
        #self.child.send(chr(int(lldt)%10))

    def Read_Data(self):
        for x in range(self.Controle['Nmr_canais']):#len(self.Data)):
            self.Data[x] = x#random.randint(0, 1)
        #msg= randomData  
        #self.Data = randomData
        self.Livetime['ADC_Conversion'] = int(0)
        self.Livetime['Time'] = float(1)
        self.Livetime['DeadTime'] = float(2)

        return 'OK'

    def dump(self): # Sinal para programa coletor enviar os dados
	    pass
        #self.child.send('i')

    def reset(self):
        #self.child.send('r')
	for k in range(int(self.Controle['Nmr_canais'])):
            self.Data[k] = 0
        

    def pause(self):
        pass
        #self.child.send('p')

    def exit(self):
        pass
        #self.child.send('q')
        #time.sleep(0.1)
        #self.child.close(force=True)

    def set_AutoGate(self):
        pass        
        #self.child.send('m')

    def set_ExternalGate(self):
        pass
        #self.child.send('n')
	



        
        
    

