# -*- coding: iso-8859-15 -*-
import numpy as np
import Tkinter as tk
import matplotlib.pyplot as plt

#########################################################################
#Classe que adiciona parâmetros necessários  para os plots dos espectros#
#########################################################################

class Grafico:

   

    def __init__(self, Nmr):
        self.Nmr_serial= Nmr #Nmr_serial do módulo da CAEN
        self.limupdtx=False # Update automático no eixo x?
        self.limupdty=False # Update automático no eixo y?
        self.limtrans=False #  Usado no botão direito
        self.Checkbutton_Plot= tk.IntVar()

        #self.limupdt=True
        #self.limctrl=False# Usado para a antiga função do botão direito
        #self.x0=0
        #self.y0=0
        #self.x1=512
        #self.y1=0
        self.pressx=0. #Variável que guarda a posição  x do click do botão direito (ZOOM)
        self.pressy=0. #Variável que guarda a posição  y do click do botão direito (ZOOM)
        #self.dx=0.
        #self.dy=0.
        self.fig= plt.figure()
        self.title=None # poderia tirar direto do grafico
        self.subplot= self.fig.add_subplot(111, axisbg='#555555') # cria os eixos do gráfico
        self.Data=None
        self.Canvas=None
        self.Frame=None # O Frame que esse gráfico se encontra
        #self.background=None
        self.x_bounds=(0,0)
        
        self.Cursor=dict(x=256,line=self.subplot.axvline(x=256,color='red'))


	#for i in range(0, 8):
	#	self.Grid = self.subplot.axvline(x=64*i, linestyle = 'dashed', color = 'black')
	#	self.Grid = self.subplot.axhline(y=32*i, linestyle = 'dashed', color = 'black')
		
    def draw_Cursor(self,Pos):
        self.Cursor['x']=(Pos)
        self.Cursor['line'].remove()
        self.Cursor['line']=self.subplot.axvline(x= self.Cursor['x'],color='red')
        self.fig.canvas.draw()


