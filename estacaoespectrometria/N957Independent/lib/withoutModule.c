#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/time.h>
#include <memory.h>

////////////////////////////////////////////
//File local defines
#define Sleep(t)  usleep((t)*1000)

#define N957_SAMPLE_NUM_BITS	13
#define N957_MAX_HISTO_SAMPLES	(1<<N957_SAMPLE_NUM_BITS)
#define SAVED_FILE "save.dat"

typedef struct {
	BOOL paused;
	BOOL start;
	BOOL error_9;
	BOOL gate;
	BOOL do_exit;
} Control; 

typedef struct {
	unsigned int paused_time;		
	unsigned int nmr_ADC_pause;
	unsigned int tempo_pause_vivo;
} Pause_info;


////////////////////////////////////////////
// Global visible variables declaration 
user_setting_data user_setting;
Control system_state;
Pause_info pause_info;
unsigned int histogram[8192];

////////////////////////////////////////////
// File local methods declaration
void init_system(N957_UINT16 *data_buff, unsigned long *histo_buff);
void start(N957_UINT32 data32);
void dump(N957_UINT32 data32, short int factor, N957_UINT16 data_buff);
void pause_system(N957_UINT32 *data32, N957_UINT16 *data_buff, unsigned long *histo_buff);
int determine_factor(int command);
void save_file();
void load_file(short int factor);

int main(int argc, char **argv)
{	
	//data of histogram	
	//N957_UINT16 *data_buff= NULL;					// read data buffer
	//unsigned long *histo_buff= NULL;				// Histogram data buffer
	short int factor = 16;

	//auxiliaries
	int w, i, command;
	char aux_string[100]; 
	
	/*unsigned int lldt=10;
	N957_ConfigROM  ROM;	//used in function not called by python code
	int ret_val= 0;						//?????*/
			
	
	//N957_UINT32 data32;	
	//unsigned int lldt;

	//N957_UINT16 data_read;
	//int32_t tot_data_read= 0;

	init_system();//data_buff, histo_buff);
	if(system_state.do_exit == TRUE)
		goto exit_point;

	/*if((argc+'0')>= 2)
    	user_setting.m_bd_num= atoi(argv[1]);
	else
		user_setting.m_bd_num=0; // tem problemas "falha de segmentação", mas eu n to usando.*/
	
	while(1)
	{
		//TRACE("flag :1 \n");
		/*if(system_state.start)
		{
			data_read= user_setting.m_bldim;
			tot_data_read+= data_read;
		}*/
		if( kbhit()) {
			command = getch();
			switch(command) {
				case 'q': //quit
					goto exit_point;
				case 's': //start acquiring
					start();//data32);
					if(system_state.do_exit == TRUE)
						goto exit_point;
					break;
				case 'r': //reset histogram						
					for(w=0; w<8192; w++)
						histogram[w] = 0;
					pause_info.paused_time = 0;
					pause_info.nmr_ADC_pause=0;
					pause_info.tempo_pause_vivo=0;				
					break;
				case 'p': //pause acquiring
					pause_system();//&data32, data_buff, histo_buff);
					if(system_state.do_exit == TRUE)
						goto exit_point;
					break;
				case 'i': // transmit info to python
					dump(factor);//data32, factor, *data_buff);
					break;
				case 'j': // set "low level detect threshold"
					//lldt= ((getchar())*10 + getchar());
					break;
				case 'z': // salva o arquivo  em "save.dat" o qual vai ser renomeado pelo programa em python //!NOT CALLED BY PYTHON CODE
					save_file();					
					break;
				case 'y': // Abre o arquivo com o nome "save.dat"
					load_file(factor);
					break;
		        case 'm': //set auto gate
                    //system_state.gate=TRUE;
            	    break;
                case 'n': //set external gate
                    //system_state.gate=FALSE;
                    break;      
				default: factor = determine_factor(command);                            

			}
		}
		
		// Calculate the histogram	
		/*for( i= 0; i< data_read; i++) 
		{			
			++histo_buff[ data_buff[i]& ( N957_MAX_HISTO_SAMPLES- 1)];
			// Log to file (if enabled)
			sprintf( aux_string, "%04d\n", data_buff[ i]);
			if (!system_state.paused)
				histogram[atoi(aux_string)+1] = histogram[atoi(aux_string)+1] +1;
			//TRACE("flag: 4 \n");

		}*/
	}

exit_point:
	return 0;
}


void init_system()//N957_UINT16 *data_buff, unsigned long *histo_buff)
{
	int w;	
	for(w=0; w<8192; w++)
		histogram[w] = 0;

	system_state.paused = FALSE;
	system_state.start = FALSE;
	system_state.error_9 = FALSE;
	system_state.gate = FALSE;
	system_state.do_exit = FALSE;

	pause_info.paused_time = 0;
	pause_info.nmr_ADC_pause = 0;
	pause_info.tempo_pause_vivo = 0;
	
	/*data_buff= malloc( user_setting.m_bldim* sizeof( N957_UINT16));
	if( data_buff== NULL)
	{
		TRACE("erro_8\n");
		system_state.do_exit = TRUE;
	}
	histo_buff= malloc( N957_MAX_HISTO_SAMPLES* sizeof( *histo_buff));
	if( histo_buff== NULL)
	{
		TRACE("erro_8\n");
		system_state.do_exit = TRUE;
	}
	memset( histo_buff, 0, N957_MAX_HISTO_SAMPLES* sizeof( *histo_buff));*/
}

void start()//N957_UINT32 data32)
{
	system_state.start= TRUE;
    system_state.paused=TRUE;        
    TRACE("OK\n");
}


void dump(short int factor)
{
	int i,w;
	unsigned int temp = 0;
		
	for(w=0; w<(8192-factor+1); w=w+factor) 
	{
	 	for(i=w; i<=(w+factor-1); i++)
			temp = temp + histogram[i];
            printf("%i", temp);
		if (w!=(8192-factor))
            printf(","); 
    	temp = 0;
	}
}

void pause_system()//N957_UINT32 *data32, N957_UINT16 *data_buff, unsigned long *histo_buff)
{
	/*data_buff= malloc( user_setting.m_bldim* sizeof( N957_UINT16)); //(N957_UINT16*)
	if( data_buff== NULL)
	{
		TRACE("erro_8\n");
		system_state.do_exit = TRUE;
	}
	histo_buff= malloc( N957_MAX_HISTO_SAMPLES* sizeof( histo_buff));//(unsigned int*)
	if( histo_buff== NULL)
	{
		TRACE("erro_8\n");
		system_state.do_exit = TRUE;
	}
	memset( &histo_buff, 0, N957_MAX_HISTO_SAMPLES* sizeof( histo_buff));	*/	
    system_state.paused= !system_state.paused;
}


int determine_factor(int command)
{
	short int factor;
	switch(command) 
	{	
		case 'a': // 8192 channels
			factor = 1;
			break;
		case 'b':// 4096 channels
			factor = 2;
			break;
		case 'c': // 2048 channels
			factor = 4;
			break;
		case 'd': // 1024 channels
			factor = 8;
			break;
		case 'e': // 512 channels
			factor = 16;
			break;
		case 'f': // 256 channels
			factor = 32;
			break;
		case 'g':// 128 channels
			factor = 64;
			break;
		case 'h': // 64 channels
			factor = 128;
			break;
		default: 
			factor = 16;
	}
	return factor;
}


void save_file()
{
	int w;	
	FILE* log = fopen(SAVED_FILE, "w+");
	for(w=0; w<8192; w++)
		fprintf(log, "%i\n", histogram[w]);
	fclose(log);
}


void load_file(short int factor)
{
	int i, w; unsigned int temp = 0; 	
	FILE* log = fopen(SAVED_FILE, "r");
	for(w=0; w<(8192-factor+1); w=w+factor)
	{
		fscanf(log, "%i", &temp);
	 	for(i=w; i<=(w+factor-1); i++)
			histogram[i] = temp/factor;
	}
	fclose(log);
}
