/***********************************************************************************************
* MODULE:     N957 Demo application
* PURPOSE:    
* WRITTEN BY: NDA
* COPYRIGHT:  CAEN S.p.A. all rights reserved
* USAGE:      
************************************************************************************************/
////////////////////////////////////////////
// File includes
////////////////////////////////////////////
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/time.h>
#include <memory.h>
#include "../include/common_defs.h"
#include "../N957/N957Lib.h"
#include "../include/user_settings.h"
#include "../include/keyb.h"

////////////////////////////////////////////
//File local defines
#define Sleep(t)  usleep((t)*1000)

#define N957_SAMPLE_NUM_BITS	13
#define N957_MAX_HISTO_SAMPLES	(1<<N957_SAMPLE_NUM_BITS)
#define SAVED_FILE "save.dat"

typedef struct {
	BOOL paused;
	BOOL start;
	BOOL error_9;
	BOOL gate;
	BOOL do_exit;
} Control; 

typedef struct {
	unsigned int paused_time;		
	unsigned int nmr_ADC_pause;
	unsigned int tempo_pause_vivo;
} Pause_info;

////////////////////////////////////////////
// File local variables declaration

////////////////////////////////////////////
// Global visible variables declaration 
user_setting_data user_setting;
Control system_state;
Pause_info pause_info;
unsigned int histogram[8192];

////////////////////////////////////////////
// File local methods declaration
void init_system(N957_UINT16 *data_buff, unsigned long *histo_buff);
void start(N957_UINT32 data32);
void dump(N957_UINT32 data32, short int factor, N957_UINT16 data_buff);
void pause_system(N957_UINT32 *data32, N957_UINT16 *data_buff, unsigned long *histo_buff);
int determine_factor(int command);
void save_file();
void load_file(short int factor);

// -----------------------------------------------------------
// get time in milliseconds since first procedure call
// -----------------------------------------------------------

/*************************************************************
* METHOD:     main
* PURPOSE:    Main program
* PARAMETERS: <in> int argc: number of arguments
*             <in> void *argv[]: arguments' list 
* RETURN:     ret_val
* USAGE:      allowed command line input parameters:
*************************************************************/
int main(int argc, char **argv)
{	
	//data of histogram	
	N957_UINT16 *data_buff= NULL;					// read data buffer
	unsigned long *histo_buff= NULL;				// Histogram data buffer
	short int factor = 16;

	//auxiliaries
	int w, i, command;
	char aux_string[100]; 
	
	/*unsigned int lldt=10;
	N957_ConfigROM  ROM;	//used in function not called by python code
	int ret_val= 0;						//?????*/
			
	
	N957_UINT32 data32;	
	unsigned int lldt;

	N957_UINT16 data_read;
	int32_t tot_data_read= 0;

	init_system(data_buff, histo_buff);
	if(system_state.do_exit == TRUE)
		goto exit_point;

	if((argc+'0')>= 2)
    	user_setting.m_bd_num= atoi(argv[1]);
	else
		user_setting.m_bd_num=0; // tem problemas "falha de segmentação", mas eu n to usando.
	
	while(1)
	{
		//TRACE("flag :1 \n");
		if(system_state.start)
		{
			data_read= user_setting.m_bldim;
	
			if( user_setting.m_max_num_samples > 0)
			{
				if(tot_data_read >= user_setting.m_max_num_samples)
				{
					//ret_val= 0;
					//goto exit_point;
				}
		
				if(user_setting.m_max_num_samples- tot_data_read< user_setting.m_bldim) 
				{
					data_read= ( N957_UINT16)(int32_t)(user_setting.m_max_num_samples - tot_data_read);
				}
			}
	
			if( N957_ReadData( user_setting.m_N957_handle, data_buff, &data_read)) 
			{         
				if(!system_state.error_9)
				{
                    system_state.error_9=TRUE;
                	TRACE("error_9\n");
				}
				// goto exit_point;
			}
            else 
            	system_state.error_9=FALSE;

			tot_data_read+= data_read;
		}
		if( kbhit()) {
			command = getch();
			switch(command) {
				case 'q': //quit
					goto exit_point;
				case 's': //start acquiring
					start(data32);
					if(system_state.do_exit == TRUE)
						goto exit_point;
					break;
				case 'r': //reset histogram						
					for(w=0; w<8192; w++)
						histogram[w] = 0;
					if( N957_SwClear(user_setting.m_N957_handle))
					{
						//ret_val= -25;
						goto exit_point;
					}
					pause_info.paused_time = 0;
					pause_info.nmr_ADC_pause=0;
					pause_info.tempo_pause_vivo=0;				
					break;
				case 'p': //pause acquiring
					pause_system(&data32, data_buff, histo_buff);
					if(system_state.do_exit == TRUE)
						goto exit_point;
					break;
				case 'i': // transmit info to python
					dump(data32, factor, *data_buff);
					break;
				case 'j': // set "low level detect threshold"
					lldt= ((getchar())*10 + getchar());
					N957_SetLLD( user_setting.m_N957_handle, lldt);
					break;
				/*case 'l':																//!NOT CALLED BY PYTHON CODE
					goto exit_point;
					//N957_SetLLD( user_setting.m_N957_handle, lldt);
					//lldt=0;
					break;*/
				case 'z': // salva o arquivo  em "save.dat" o qual vai ser renomeado pelo programa em python //!NOT CALLED BY PYTHON CODE
					save_file();					
					break;
				case 'y': // Abre o arquivo com o nome "save.dat"
					load_file(factor);
					break;
		        /*case 'k':																			//!NOT CALLED BY PYTHON CODE
					TRACE("OK\n");

					N957_GetConfigROM( user_setting.m_N957_handle, &ROM);

					TRACE1("%u \n" ,ROM.m_serial);					
					break;*/
                case 'm': //set auto gate     				
	                N957_SetAcquisitionMode(user_setting.m_N957_handle, N957ControlModeAuto);
                    system_state.gate=TRUE;
            	    break;
                case 'n': //set external gate
    		        N957_SetAcquisitionMode(user_setting.m_N957_handle, N957ControlModeExtGate);
                    system_state.gate=FALSE;
                    break;      
				default: factor = determine_factor(command);                            

			}
		}
			//TRACE("flag: 3 \n");

		// Calculate the histogram	
		for( i= 0; i< data_read; i++) 
		{			
			++histo_buff[ data_buff[i]& ( N957_MAX_HISTO_SAMPLES- 1)];
			// Log to file (if enabled)
			sprintf( aux_string, "%04d\n", data_buff[ i]);
			if (!system_state.paused)
				histogram[atoi(aux_string)+1] = histogram[atoi(aux_string)+1] +1;
			//TRACE("flag: 4 \n");

		}
	}

	if(user_setting.m_debug)
	{
		// clear software convertion
		if( N957_SetSwConvFlag( user_setting.m_N957_handle, N957_FALSE))
		{
			TRACE("erro_20\n");
			goto exit_point;
		}
	}

exit_point:
	//stop acquisition
	if(N957_StopAcquire( user_setting.m_N957_handle))
	{
		TRACE("erro_24\n");
	}

	if(N957_SwClear(user_setting.m_N957_handle))
	{
		TRACE("erro_25\n");
	}	

	//close modules
	user_settings_close( &user_setting);

	return 0;
}

void init_system(N957_UINT16 *data_buff, unsigned long *histo_buff)
{
	int w;	
	for(w=0; w<8192; w++)
		histogram[w] = 0;

	system_state.paused = FALSE;
	system_state.start = FALSE;
	system_state.error_9 = FALSE;
	system_state.gate = FALSE;
	system_state.do_exit = FALSE;

	pause_info.paused_time = 0;
	pause_info.nmr_ADC_pause = 0;
	pause_info.tempo_pause_vivo = 0;
                               
	memset( &user_setting, 0, sizeof( user_setting));
	
	// init user setting module
	if( !user_settings_open( &user_setting))
	{
		TRACE("erro_1\n");
		system_state.do_exit = TRUE;
	}
	
	data_buff= malloc( user_setting.m_bldim* sizeof( N957_UINT16));
	if( data_buff== NULL)
	{
		TRACE("erro_8\n");
		system_state.do_exit = TRUE;
	}
	histo_buff= malloc( N957_MAX_HISTO_SAMPLES* sizeof( *histo_buff));
	if( histo_buff== NULL)
	{
		TRACE("erro_8\n");
		system_state.do_exit = TRUE;
	}
	memset( histo_buff, 0, N957_MAX_HISTO_SAMPLES* sizeof( *histo_buff));

   
}

void start(N957_UINT32 data32)
{
	/// input parameter check
	if( !parse_config_file( &user_setting))
	{
		TRACE("erro_2\n");
        N957_End(user_setting.m_N957_handle);

	}
	// now board handle is valid and we can start calling boards API
	// Get firmware revision
	char fw_rev[10];
	if( ( N957_GetFWRelease( user_setting.m_N957_handle, fw_rev, 10)))
	{
		TRACE("erro_3");
	}

	if( N957_GetScaler(user_setting.m_N957_handle, &data32))
	{
		TRACE("erro_5");
	}		

	if(N957_StartAcquire( user_setting.m_N957_handle, user_setting.m_mode))
	{
		TRACE("erro_6");
	}

	if( user_setting.m_debug)
	{
		if( (N957_SetSwConvFlag( user_setting.m_N957_handle, N957_TRUE)))
		{
			TRACE("erro_7");
		}
	}
    if((N957_StopAcquire( user_setting.m_N957_handle)))
	{
		TRACE("erro_24\n");
		system_state.do_exit = TRUE;
	}
	system_state.start= TRUE;
    system_state.paused=TRUE;
	TRACE("OK\n");
}

void dump(N957_UINT32 data32, short int factor, N957_UINT16 data_buff)
{
	int i,w;
	unsigned int temp = 0;
		
	TRACE("OK\n");
	TRACE("[");
	for(w=0; w<(8192-factor+1); w=w+factor) 
	{
	 	for(i=w; i<=(w+factor-1); i++)
			temp = temp + histogram[i];
		TRACE1("%i",temp);
		if (w!=(8192-factor)) 
			TRACE(",");
		temp = 0;
		//JULIO w=w+factor;
	}
	TRACE("]\n");
	N957_GetScaler(user_setting.m_N957_handle, &data32);
	TRACE1("%d\n",data32 + pause_info.nmr_ADC_pause);
	N957_GetTimer(user_setting.m_N957_handle, &data32);
	TRACE1("%d\n",data32 +pause_info.paused_time);
	N957_GetLiveTime(user_setting.m_N957_handle, &data32);
	TRACE1("%d\n",data32 +pause_info.tempo_pause_vivo);

	/*N957_UINT16 tamanho;
	N957_GetBufferOccupancy( user_setting.m_N957_handle, &tamanho);

	TRACE1("%d\n",tamanho);*/  // mostra se quntas contagens tem no buffer	
}

void pause_system(N957_UINT32 *data32, N957_UINT16 *data_buff, unsigned long *histo_buff)
{
	if(!system_state.paused)
	{		
		if((N957_StopAcquire( user_setting.m_N957_handle)))
		{
			TRACE("erro_24\n");
			system_state.do_exit = TRUE;
		}
	}
	else //if(system_state.paused) 
	{
		N957_GetScaler(user_setting.m_N957_handle, data32);
		pause_info.nmr_ADC_pause= pause_info.nmr_ADC_pause + *data32;					
		N957_GetTimer(user_setting.m_N957_handle, data32);
		pause_info.paused_time = pause_info.paused_time + *data32;
		N957_GetLiveTime(user_setting.m_N957_handle, data32);
		pause_info.tempo_pause_vivo = pause_info.tempo_pause_vivo + *data32;
          		
		if((N957_StartAcquire(user_setting.m_N957_handle,user_setting.m_mode)))
		{
			TRACE("erro_6\n");
			system_state.do_exit = TRUE;
		}
        if (system_state.gate==FALSE)
              N957_SetAcquisitionMode( user_setting.m_N957_handle, N957ControlModeExtGate);							
		if( user_setting.m_debug) {
			if((N957_SetSwConvFlag( user_setting.m_N957_handle, N957_TRUE)))
			{
				TRACE("erro_7\n");
				system_state.do_exit = TRUE;
			}
		}	
		data_buff= malloc( user_setting.m_bldim* sizeof( N957_UINT16)); //(N957_UINT16*)
		if( data_buff== NULL)
		{
			TRACE("erro_8\n");
			system_state.do_exit = TRUE;
		}
		histo_buff= malloc( N957_MAX_HISTO_SAMPLES* sizeof( histo_buff));//(unsigned int*)
		if( histo_buff== NULL)
		{
			TRACE("erro_8\n");
			system_state.do_exit = TRUE;
		}
		memset( &histo_buff, 0, N957_MAX_HISTO_SAMPLES* sizeof( histo_buff));
	
	}					
	system_state.paused= !system_state.paused;
}

int determine_factor(int command)
{
	short int factor;
	switch(command) 
	{	
		case 'a': // 8192 channels
			factor = 1;
			break;
		case 'b':// 4096 channels
			factor = 2;
			break;
		case 'c': // 2048 channels
			factor = 4;
			break;
		case 'd': // 1024 channels
			factor = 8;
			break;
		case 'e': // 512 channels
			factor = 16;
			break;
		case 'f': // 256 channels
			factor = 32;
			break;
		case 'g':// 128 channels
			factor = 64;
			break;
		case 'h': // 64 channels
			factor = 128;
			break;
		default: 
			factor = 16;
	}
	return factor;
}

void save_file()
{
	int w;	
	FILE* log = fopen(SAVED_FILE, "w+");
	for(w=0; w<8192; w++)
		fprintf(log, "%i\n", histogram[w]);
	fclose(log);
}

void load_file(short int factor)
{
	int i, w; unsigned int temp = 0; 	
	FILE* log = fopen(SAVED_FILE, "r");
	for(w=0; w<(8192-factor+1); w=w+factor)
	{
		fscanf(log, "%i", &temp);
	 	for(i=w; i<=(w+factor-1); i++)
			histogram[i] = temp/factor;
	}
	fclose(log);
}
