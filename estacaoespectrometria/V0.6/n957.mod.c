#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xae141548, "module_layout" },
	{ 0x95fef88, "usb_register_driver" },
	{ 0x93fca811, "__get_free_pages" },
	{ 0x50ac1805, "usb_register_dev" },
	{ 0x105e2727, "__tracepoint_kmalloc" },
	{ 0xf4c9dacc, "kmem_cache_alloc" },
	{ 0xfc2358d1, "kmalloc_caches" },
	{ 0x14e158a5, "usb_deregister_dev" },
	{ 0x5f8bdac9, "dev_set_drvdata" },
	{ 0x2cb6407f, "dev_get_drvdata" },
	{ 0x37a0cba, "kfree" },
	{ 0xb72397d5, "printk" },
	{ 0xd3f74cf1, "interruptible_sleep_on_timeout" },
	{ 0x7908292f, "usb_bulk_msg" },
	{ 0xa4709493, "per_cpu__current_task" },
	{ 0x446da885, "mutex_unlock" },
	{ 0xc7423ae, "mutex_lock" },
	{ 0xf0fdf6cb, "__stack_chk_fail" },
	{ 0x2da418b5, "copy_to_user" },
	{ 0xe914e41e, "strcpy" },
	{ 0xf2a644fb, "copy_from_user" },
	{ 0x728d61cc, "__mutex_init" },
	{ 0xffc7c184, "__init_waitqueue_head" },
	{ 0x3f1899f1, "up" },
	{ 0x748caf40, "down" },
	{ 0x669ec208, "usb_deregister" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=usbcore";

MODULE_ALIAS("usb:v0547p1002d*dc*dsc*dp*ic*isc*ip*");
