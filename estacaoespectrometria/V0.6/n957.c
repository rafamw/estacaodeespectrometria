
/*
 * CAEN n957 USB/NIM Bridge driver - 0.6
 *
 * Written by Stefano Coluccini (s.coluccini@caen.it) - CAEN SpA
 *
 * based on rio500.c by Cesar Miquel (miquel@df.uba.ar)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 *
 *   TODO
 *   - Precise identification of the device based on strings.
 *   
 * Changelog:
 * 
 *	March 2010 rev 0.6
 *	   - removed minor check for kernel 2.6
 *
 *   April 2007 rev 0.5
 *     - Branched from v1718 CAEN USB/VME Bridge
 *     - !!! WARNING !!! conficts with usbtest driver. Remove it!
 */

#undef DEBUG  
 
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/random.h>
#include <linux/poll.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/usb.h>
#include <linux/smp_lock.h>
#ifdef CONFIG_DEVFS_FS
#include <linux/devfs_fs_kernel.h>
#endif

/*
 * Version Information
 */
#define DRIVER_VERSION "v0.6"
#define DRIVER_AUTHOR "CAEN COmputing Division <support.computing@caen.it>"
#define DRIVER_DESC "CAEN N957 USB/NIM Bridge driver"

#define N957_MINOR_BASE   210

/* stall/wait timeout for n957 */
#define NAK_TIMEOUT (HZ)

#define IBUF_SIZE 0x10000	/* 64 kbytes */

/* Size of the n957 buffer */
#define OBUF_SIZE 0x10000	/* 64 kbytes */

/* we can have up to this number of devices plugged in at once */
#define MAX_DEVICES	16
//
// Rev 0.2: added ini
#include <linux/version.h>

#ifndef VERSION
	#define VERSION(ver,rel,seq) (((ver)<<16) | ((rel)<<8) | (seq))
#endif	

#if LINUX_VERSION_CODE >= VERSION(2,5,0)
	
	#define USB_ST_TIMEOUT			(-ETIMEDOUT)
	#define USB_ST_DATAUNDERRUN		(-EREMOTEIO)
#endif

// Rev. 0.4
#if LINUX_VERSION_CODE >= VERSION(2,6,11)
        #include <linux/mutex.h>		// for unlocked ioctl interface
#endif

#include "n957.h"
//
// Rev 0.2: added end

struct n957_usb_data {
    struct usb_device *n957_dev;   /* init: probe_n957 */
#if LINUX_VERSION_CODE < VERSION(2,5,0)
    devfs_handle_t devfs;           /* devfs device */
#endif		
    unsigned int ifnum;             /* Interface number of the USB device */
    int open_count;                 /* number of users */
    int present;                    /* Device is present on the bus */
    int minor;                      /* minor number for the device */
    char *obuf, *ibuf;              /* transfer buffers */
    char bulk_in_ep, bulk_out_ep;   /* Endpoint assignments */
    wait_queue_head_t wait_q;       /* for timeouts */
	struct semaphore lock;          /* general race avoidance */
// Rev 0.4
#if LINUX_VERSION_CODE >= VERSION(2,6,11)
    struct mutex           ioctl_lock;
#endif
};

#if LINUX_VERSION_CODE < VERSION(2,5,0)
	extern devfs_handle_t usb_devfs_handle;	/* /dev/usb dir. */
#endif

/* array of pointers to our devices that are currently connected */
static struct n957_usb_data *minor_table[MAX_DEVICES];

/* lock to protect the minor_table structure */
static DECLARE_MUTEX (minor_table_mutex);
//
//Rev 0.2
// static n957_delete(struct n957_usb_data *n957)
static void n957_delete(struct n957_usb_data *n957)
{
	minor_table[n957->minor] = NULL;
	if( n957->ibuf != NULL )
		kfree(n957->ibuf);
	if( n957->obuf != NULL )
		kfree(n957->obuf);
	kfree(n957);
}

// // Rev. 0.4
#if LINUX_VERSION_CODE >= VERSION(2,6,11)
static long unlocked_ioctl_n957(struct file *, unsigned int, unsigned long);
#endif

static int open_n957(struct inode *inode, struct file *file)
{
	struct n957_usb_data *n957 = NULL;
	int subminor;
//Dbg
//info( "n957 opening...\n");

#if LINUX_VERSION_CODE <= VERSION(2,5,0)
	subminor = MINOR(inode->i_rdev) - N957_MINOR_BASE;
	if( (subminor < 0) || (subminor > MAX_DEVICES) )
		return -ENODEV;
#else
	subminor = MINOR(inode->i_rdev);
#endif
	down(&minor_table_mutex);

	n957 = minor_table[subminor];
	if( n957 == NULL ) {
		up(&minor_table_mutex);
		return -ENODEV;
	}

	down(&n957->lock);

	up(&minor_table_mutex);

	n957->open_count++;

	init_waitqueue_head(&n957->wait_q);

// Rev. 1.6
#if LINUX_VERSION_CODE >= VERSION(2,6,11)
        mutex_init(&n957->ioctl_lock);
#endif
	file->private_data = n957;

	up(&n957->lock);

//	info("n957 opened.");

//Dbg
//info( "n957 opened...\n");

	return 0;
}

static int close_n957(struct inode *inode, struct file *file)
{
	struct n957_usb_data *n957 = (struct n957_usb_data *)file->private_data;
	int retval = 0;
	if( n957 == NULL ) {
		err("Close: object is null");
		return -ENODEV;
	}
	down(&minor_table_mutex);
	down(&n957->lock);

	if( n957->open_count <= 0 ) {
		err("Device not opened");
		retval = -ENODEV;
		goto exit_not_opened;
	}

	if( n957->n957_dev == NULL ) {
		/* the device was unplugged before the file was released */
		up(&n957->lock);
		n957_delete(n957);
		up(&minor_table_mutex);
		return 0;
	}

	n957->open_count--;

exit_not_opened:
	up(&(n957->lock));
	up(&minor_table_mutex);

//	info("n957 closed.");
//Dbg
//info( "n957 close...\n");

	return retval;
}

static int
ioctl_n957(struct inode *inode, struct file *file, unsigned int cmd,
            unsigned long arg)
{
//
// rev 0.2
	int ret= 0;
	struct n957_usb_data *n957;
	
	n957 = (struct n957_usb_data *)file->private_data;
	down(&(n957->lock));
    /* Sanity check to make sure n957 is connected, powered, etc */
    if ( n957 == NULL ||
         n957->present == 0 ||
         n957->n957_dev == NULL )
	{
		up(&(n957->lock));
		return -ENOTTY;
	}

    switch (cmd) {
        case N957_IOCTL_REV:
			{
				n957_rev_t rev;
                if( copy_from_user(&rev, (n957_rev_t *)arg, sizeof(rev)) > 0 ) {
                        ret = -EFAULT;
                        break;
                }
				strcpy( rev.rev_buf, DRIVER_VERSION);
                if( copy_to_user((n957_rev_t *)arg, &rev, sizeof(rev)) > 0) {
                        ret = -EFAULT;
                        break;
                }
			}
	        break;
		default:
			ret= -ENOTTY;
			break;
    }
	up(&(n957->lock));
    return ret;
}

// Rev 0.4
/*
        ----------------------------------------------------------------------

        unlocked_ioctl_n957 (Called in preference to ioctl_n957 on newer kernels)

        ----------------------------------------------------------------------
*/

#if LINUX_VERSION_CODE >= VERSION(2,6,11)
static long unlocked_ioctl_n957(struct file *file, unsigned int cmd, unsigned long arg)
{
	struct inode *inode = file->f_dentry->d_inode;
        struct n957_usb_data *s = (struct n957_usb_data *)file->private_data;
	long ret;

	/* ioctl() calls can cause the Big Kernel Lock (BKL) to be taken, which
	 * can have significant performance penalties system-wide.  By providing
	 * an unlocked ioctl() method the BKL will not be taken, but the driver
	 * becomes responsible for its own locking.  Furthermore, the lock can be
	 * broken down per A2818 so that multiple threads accessing different CONET
	 * chains do not contend with one another during ioctl() calls.
	 */
	mutex_lock(&s->ioctl_lock);
	ret = (long) ioctl_n957(inode, file, cmd, arg);
	mutex_unlock(&s->ioctl_lock);

	return ret;
}
#endif

static ssize_t
write_n957(struct file *file, const char *buffer,
            size_t count, loff_t * ppos)
{
	struct n957_usb_data *n957;

	unsigned long copy_size;
	unsigned long bytes_written = 0;
	unsigned int partial;

	int result = 0;
	int maxretry;
	int errn = 0;

	n957 = (struct n957_usb_data *)file->private_data;

	down(&(n957->lock));
        /* Sanity check to make sure n957 is connected, powered, etc */
        if ( n957 == NULL ||
             n957->present == 0 ||
             n957->n957_dev == NULL )
	{
		up(&(n957->lock));
		return -ENODEV;
	}

	do {
		unsigned long thistime;
		char *obuf = n957->obuf;

		thistime = copy_size =
		    (count >= OBUF_SIZE) ? OBUF_SIZE : count;
		if (copy_from_user(n957->obuf, buffer, copy_size)) {
			errn = -EFAULT;
			goto error;
		}
//		maxretry = 5;
		maxretry = 1;   // TEMP - 1 volta e' sufficiente ?
		while (thistime) {
			if (!n957->n957_dev) {
				errn = -ENODEV;
				goto error;
			}
			if (signal_pending(current)) {
				up(&(n957->lock));
				return bytes_written ? bytes_written : -EINTR;
			}

			result = usb_bulk_msg(n957->n957_dev,
					 usb_sndbulkpipe(n957->n957_dev, 2),
//					 obuf, thistime, &partial, 5 * HZ);
					 obuf, thistime, &partial, 1 * HZ);     // TEMP - 1 sec basta?

			dbg("write stats: result:%d thistime:%lu partial:%u",
			     result, thistime, partial);

			if (result == USB_ST_TIMEOUT) {	/* NAK - so hold for a while */
				if (!maxretry--) {
					errn = -ETIME;
					goto error;
				}
				interruptible_sleep_on_timeout(&n957-> wait_q, NAK_TIMEOUT);
				continue;
//Rev 0.2				
//			} else if (!result & partial) {
			} else if (!result && partial) {
				obuf += partial;
				thistime -= partial;
			} else
				break;
		};
		if (result) {
			err("Write Whoops - %x", result);
			errn = -EIO;
			goto error;
		}
		bytes_written += copy_size;
		count -= copy_size;
		buffer += copy_size;
	} while (count > 0);

	up(&(n957->lock));

	return bytes_written ? bytes_written : -EIO;

error:
	up(&(n957->lock));
	return errn;
}

static ssize_t
read_n957(struct file *file, char *buffer, size_t count, loff_t * ppos)
{
	struct n957_usb_data *n957;
	ssize_t read_count;
	unsigned int partial;
	int this_read;
	int result;
//	int maxretry = 10;              // TEMP - Prova per abortire prima 
	int maxretry = 1;	
        char *ibuf;

	n957 = (struct n957_usb_data *)file->private_data;

	down(&(n957->lock));
	/* Sanity check to make sure n957 is connected, powered, etc */
        if ( n957 == NULL ||
             n957->present == 0 ||
             n957->n957_dev == NULL )
	{
		up(&(n957->lock));
		return -ENODEV;
	}

	ibuf = n957->ibuf;

	read_count = 0;

	while (count > 0) {
		if (signal_pending(current)) {
			up(&(n957->lock));
			return read_count ? read_count : -EINTR;
		}
		if (!n957->n957_dev) {
			up(&(n957->lock));
			return -ENODEV;
		}
		this_read = (count >= IBUF_SIZE) ? IBUF_SIZE : count;

		result = usb_bulk_msg(n957->n957_dev,
				      usb_rcvbulkpipe(n957->n957_dev, 6),
				      ibuf, this_read, &partial,
//				      (int) (HZ * 8));
				      (int) (HZ * 1)); // TEMP - 1 secondo credo che basti 

		dbg("read stats: result:%d this_read:%u partial:%u",
		       result, this_read, partial);

		if (partial) {
			count = this_read = partial;
		} else if (result == USB_ST_TIMEOUT || result == 15) {	/* FIXME: 15 ??? */
			if (!maxretry--) {
				up(&(n957->lock));
				err("read_n957: maxretry timeout");
				return -ETIME;
			}
			interruptible_sleep_on_timeout(&n957->wait_q,
						       NAK_TIMEOUT);
			continue;
		} else if (result != USB_ST_DATAUNDERRUN) {
			up(&(n957->lock));
			err("Read Whoops - result:%u partial:%u this_read:%u",
			     result, partial, this_read);
			return -EIO;
		} else {
			up(&(n957->lock));
			return (0);
		}

		if (this_read) {
			if (copy_to_user(buffer, ibuf, this_read)) {
				up(&(n957->lock));
				return -EFAULT;
			}
			count -= this_read;
			read_count += this_read;
			buffer += this_read;
		}
	}
	up(&(n957->lock));
	return read_count;
}

static struct
file_operations usb_n957_fops = {
	owner:		THIS_MODULE,            
	read:		read_n957,
	write:		write_n957,
	ioctl:		ioctl_n957,
// Rev. 0.4
#if LINUX_VERSION_CODE >= VERSION(2,6,11)
        unlocked_ioctl: unlocked_ioctl_n957,
#endif
	open:		open_n957,
	release:	close_n957,
};

#if LINUX_VERSION_CODE >= VERSION(2,5,0)

static struct usb_class_driver usb_n957_class = {
  .name = "n957_%d",
  .fops = &usb_n957_fops ,
#if LINUX_VERSION_CODE <= VERSION(2,6,13)
  .mode = S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP ,
#endif
  .minor_base = N957_MINOR_BASE ,
};

	
static int probe_n957(struct usb_interface *intf,
                         const struct usb_device_id *id)
#else
static void *probe_n957(struct usb_device *dev, unsigned int ifnum,
                         const struct usb_device_id *id)
#endif						 
{

#if LINUX_VERSION_CODE >= VERSION(2,5,0)
	struct usb_device *dev = interface_to_usbdev(intf);
	int retval= 0;
#endif
	struct n957_usb_data *n957 = NULL;
// Rev 0.2	
//	char name[12];
	int minor;

	printk("CAEN n957 found at address %d", dev->devnum);

	down(&minor_table_mutex);
	for( minor = 0; minor < MAX_DEVICES; ++minor ) {
		if( minor_table[minor] == NULL )
			break;
	}
	if( minor >= MAX_DEVICES ) {
		printk("Too many devices");
		goto exit;
	}

	n957 = kmalloc(sizeof(struct n957_usb_data), GFP_KERNEL);
	if (n957 == NULL) {
		err("Out of memory");
		goto exit;
	}
	memset(n957, 0x00, sizeof(*n957));
	minor_table[minor] = n957;
#if LINUX_VERSION_CODE >= VERSION(2,5,0)
	if( usb_register_dev(intf, &usb_n957_class))
  	{
    	err("probe_n957: Not able to get a minor for this device.");
		goto error;
	}
#endif	
	n957->present = 1;
	n957->n957_dev = dev;
	n957->minor = minor;

	if (!(n957->obuf = (char *) kmalloc(OBUF_SIZE, GFP_KERNEL))) {
		err("probe_n957: Not enough memory for the output buffer");
		goto error;
	}
	dbg("probe_n957: obuf address:%p", n957->obuf);

	if (!(n957->ibuf = (char *) kmalloc(IBUF_SIZE, GFP_KERNEL))) {
		err("probe_n957: Not enough memory for the input buffer");
		goto error;
	}
	dbg("probe_n957: ibuf address:%p", n957->ibuf);

#if LINUX_VERSION_CODE < VERSION(2,5,0)
	n957->devfs = devfs_register(usb_devfs_handle, "n957_0",
				      DEVFS_FL_DEFAULT, USB_MAJOR,
				      N957_MINOR_BASE + n957->minor,
				      S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP |
				      S_IWGRP, &usb_n957_fops, NULL);
	if (n957->devfs == NULL)
		dbg("probe_n957: device node registration failed");

	init_MUTEX(&(n957->lock));
#else

	init_MUTEX(&(n957->lock));
	usb_set_intfdata (intf, n957);
	
#endif	
	goto exit;

error:
#if LINUX_VERSION_CODE >= VERSION(2,5,0)
	usb_deregister_dev(intf, &usb_n957_class);
	retval= -ENOMEM;

#endif
	n957_delete(n957);
	n957 = NULL;

exit:
	up(&minor_table_mutex);
#if LINUX_VERSION_CODE >= VERSION(2,5,0)
	return retval;
#else
	return n957;
#endif	
}

#if LINUX_VERSION_CODE >= VERSION(2,5,0)
static void disconnect_n957(struct usb_interface *intf)
#else
static void disconnect_n957(struct usb_device *dev, void *ptr)
#endif
{
#if LINUX_VERSION_CODE >= VERSION(2,5,0)
	struct n957_usb_data *n957 = usb_get_intfdata (intf);
#else
	struct n957_usb_data *n957 = (struct n957_usb_data *) ptr;
#endif
	int minor;

	down(&minor_table_mutex);
	down(&(n957->lock));

	minor = n957->minor;

#if LINUX_VERSION_CODE >= VERSION(2,5,0)
	usb_set_intfdata (intf, NULL);
	if (n957) {
		usb_deregister_dev(intf, &usb_n957_class);
//rev 0.4: BugFix
//  		down(&(n957->lock));
		if (n957->open_count != 0) {
			/* better let it finish - the release will do whats needed */
			n957->n957_dev = NULL;
			up(&(n957->lock));
		} else {
			up(&(n957->lock));
			n957_delete(n957);
		}
	}  
#else
	devfs_unregister(n957->devfs);

	if (n957->open_count != 0) {
		/* better let it finish - the release will do whats needed */
		n957->n957_dev = NULL;
		up(&(n957->lock));
	} else {
		up(&(n957->lock));
		n957_delete(n957);
	}
#endif

	printk("CAEN #%d n957 disconnected.", minor);

	up(&minor_table_mutex);
}

static struct usb_device_id n957_table [] = {
	{ USB_DEVICE(0x0547, 0x1002) }, 		/* CAEN n957 */
	{ }										/* Terminating entry */
};

MODULE_DEVICE_TABLE (usb, n957_table);

// Rev 0.4
static struct usb_driver n957_driver = {
#if LINUX_VERSION_CODE >= VERSION(2,5,0) & LINUX_VERSION_CODE < VERSION(2,6,15)
	owner:		THIS_MODULE,
#endif	
	name:		"n957",
	probe:		probe_n957,
	disconnect:	disconnect_n957,
#if LINUX_VERSION_CODE < VERSION(2,5,0)
	fops:		&usb_n957_fops,
	minor:		N957_MINOR_BASE,
#endif	
	id_table:	n957_table,
};

#if LINUX_VERSION_CODE >= VERSION(2,5,0)
static int __init usb_n957_init(void)
#else
static int usb_n957_init(void)
#endif
{
	if (usb_register(&n957_driver) < 0)
		return -1;
	printk(DRIVER_VERSION ":" DRIVER_DESC);

	return 0;
}


#if LINUX_VERSION_CODE >= VERSION(2,5,0)
static void __exit usb_n957_cleanup(void)
#else
static void usb_n957_cleanup(void)
#endif
{
//Dbg
//printk( "usb_n957_cleanup enter ...\n");
	usb_deregister(&n957_driver);
//Dbg
//printk( "usb_n957_cleanup leave ...\n");
}

module_init(usb_n957_init);
module_exit(usb_n957_cleanup);

MODULE_AUTHOR( DRIVER_AUTHOR );
MODULE_DESCRIPTION( DRIVER_DESC );
MODULE_LICENSE("GPL");

