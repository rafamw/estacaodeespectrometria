/***********************************************************************************************
* MODULE:     N957 Demo application
* PURPOSE:    
* WRITTEN BY: NDA
* COPYRIGHT:  CAEN S.p.A. all rights reserved
* USAGE:      
************************************************************************************************/
////////////////////////////////////////////
// File includes
////////////////////////////////////////////
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/time.h>
#define Sleep(t)  usleep((t)*1000)
#include <memory.h>
#include "common_defs.h"
#include "./include/N957Lib.h"
#include "user_settings.h"
#include "keyb.h"


////////////////////////////////////////////
// File local defines
////////////////////////////////////////////
#define N957_SAMPLE_NUM_BITS	13
#define N957_MAX_HISTO_SAMPLES	(1<<N957_SAMPLE_NUM_BITS)

////////////////////////////////////////////
// File local variables declaration
////////////////////////////////////////////
const char* HISTO_FILENAME= "histo.dat";

////////////////////////////////////////////
// Global visible variables declaration
////////////////////////////////////////////



////////////////////////////////////////////
// File local methods declaration
////////////////////////////////////////////
// ---------------------------------------------------------------------------
// get time in milliseconds since first procedure call
// ---------------------------------------------------------------------------



/**************************************************
**************************************************/

/***********************************************************************************************
* METHOD:     main
* PURPOSE:    Main program
* PARAMETERS: <in> int argc: number of arguments
*             <in> void *argv[]: arguments' list 
* RETURN:     
* USAGE:      allowed command line input parameters:
************************************************************************************************/
int main(int argc, char **argv) 

{	
        

	

	int w;
	unsigned int histograma[8192];
	unsigned int temp = 0, lldt=10;
	unsigned int tempo_pause = 0;
	unsigned int nmr_ADC_pause=0;
	unsigned int tempo_pause_vivo=0;
	FILE* log;	
	short int fator = 16;
	N957_UINT32 data32;
	int ret_val= 0;
	N957_ConfigROM  ROM;
							// procedure exit value
	user_setting_data user_setting;					// user settings
	N957_UINT16 *data_buff= NULL;					// read data buffer
	unsigned long *histo_buff= NULL;				// Histogram data buffer
	int32_t tot_data_read= 0;
	int ii, i;
	char aux_string[ 100]; 
	BOOL paused= FALSE;                                             //organizar isso em forma de struct CONTROL
	BOOL do_exit= FALSE;
        BOOL start=FALSE;
        BOOL erro_9=FALSE;
        BOOL GATE=TRUE;



	for(w=0; w<8192; w++)
		histograma[w] = 0;

	/////////////////////////////////////////
	// Demo application specific
	/////////////////////////////////////////

	memset( &user_setting, 0, sizeof( user_setting));
	


	// init user setting module
	if( !user_settings_open( &user_setting))
	{
		TRACE("erro_1\n");
		goto exit_point;
	}

	//
	

	
	data_buff= malloc( user_setting.m_bldim* sizeof( N957_UINT16));
	if( data_buff== NULL)
	{
		TRACE("erro_8\n");
		goto exit_point;
	}
	histo_buff= malloc( N957_MAX_HISTO_SAMPLES* sizeof( *histo_buff));
	if( histo_buff== NULL)
	{
		TRACE("erro_8\n");
		goto exit_point;
	}
	memset( histo_buff, 0, N957_MAX_HISTO_SAMPLES* sizeof( *histo_buff));

        if((argc+'0')>= 2)
        	user_setting.m_bd_num= atoi(argv[1]);
	else
		user_setting.m_bd_num=0; // tem problemas "falha de segmentação", mas eu n to usando.


	do_exit= FALSE;
	
	while( !do_exit) {
		//TRACE("flag :1 \n");

		
		N957_UINT16 data_read;

		if(start)
		{
			data_read= user_setting.m_bldim;
	
			if( user_setting.m_max_num_samples> 0){
				if( tot_data_read>= user_setting.m_max_num_samples) {
					ret_val= 0;
					//goto exit_point;
				}
		
				if( user_setting.m_max_num_samples- tot_data_read< user_setting.m_bldim) {
					data_read= ( N957_UINT16)(int32_t)(user_setting.m_max_num_samples - tot_data_read);
				}
			}
	
			//
			// ReadData

			if( N957_ReadData( user_setting.m_N957_handle, data_buff, &data_read)) {
                                
				if(!erro_9)
				{
	                                erro_9=TRUE;
                                	TRACE("erro_9\n");
				}
				//goto exit_point;
			}
                        else 
                            erro_9=FALSE;
			tot_data_read+= data_read;
		}
			if( kbhit()) {
				switch( getch()) {
				case 'q':
				case 'Q':
					 goto exit_point;
				case 's':
				case 'S':
					/// input parameter check
					if( !parse_config_file( &user_setting))
					{
						TRACE("erro_2\n");
                                                N957_End(user_setting.m_N957_handle);
						break;

					}

					/////////////////////////////////////////
					// Library specific
					/////////////////////////////////////////


					//
					// now board handle is valid and we can start calling boards API
					{
						// Get firmware revision
						char fw_rev[10];
						if( ( N957_GetFWRelease( user_setting.m_N957_handle, fw_rev, 10)))
						{
							TRACE("erro_3");
							break;
						}
					}
	
					if( N957_GetScaler(user_setting.m_N957_handle, &data32))
					{
						TRACE("erro_5");
                                		break;
					}		
	
					if(N957_StartAcquire( user_setting.m_N957_handle, user_setting.m_mode))
					{
						TRACE("erro_6");
						break;
					}
		
					if( user_setting.m_debug) {
						if( (N957_SetSwConvFlag( user_setting.m_N957_handle, N957_TRUE)))
						{
	
							TRACE("erro_7");
							break;
						}
					}
                                        if((N957_StopAcquire( user_setting.m_N957_handle)))
				        {
							TRACE("erro_24\n");
							goto exit_point;

				        }
                                        start= TRUE;
                                        paused=TRUE;
					TRACE("OK\n");
					break;

				case 'r':
				case 'R':
					// reset the histogram here
					//memset( histo_buff, 0, N957_MAX_HISTO_SAMPLES* sizeof( *histo_buff));
					for(w=0; w<8192; w++)
							histograma[w] = 0;
					if( N957_SwClear(user_setting.m_N957_handle))
					{
						ret_val= -25;
						goto exit_point;
					}
					tempo_pause = 0;
					nmr_ADC_pause=0;
					tempo_pause_vivo=0;				
					break;
			
				case 'p':					
				case 'P': // pausa
					if(!paused)
					{
											
						if((N957_StopAcquire( user_setting.m_N957_handle)))
						{
							TRACE("erro_24\n");
							goto exit_point;
						}
						
					}
					if(paused) // se esta pausado
					{
						N957_GetScaler(user_setting.m_N957_handle, &data32);
						nmr_ADC_pause= nmr_ADC_pause + data32;					
						N957_GetTimer(user_setting.m_N957_handle, &data32);
						tempo_pause = tempo_pause + data32;
						N957_GetLiveTime(user_setting.m_N957_handle, &data32);
						tempo_pause_vivo = tempo_pause_vivo + data32;
                          		
						if((N957_StartAcquire(user_setting.m_N957_handle,user_setting.m_mode)))
						{
							TRACE("erro_6\n");
							goto exit_point;
						}
                                                if (GATE==FALSE)
                                                      N957_SetAcquisitionMode( user_setting.m_N957_handle, N957ControlModeExtGate);							
						if( user_setting.m_debug) {
							if((N957_SetSwConvFlag( user_setting.m_N957_handle, N957_TRUE)))
							{
								TRACE("erro_7\n");
								goto exit_point;
							}
						}	
						data_buff= malloc( user_setting.m_bldim* sizeof( N957_UINT16));
						if( data_buff== NULL)
						{
							TRACE("erro_8\n");
							goto exit_point;
						}
						histo_buff= malloc( N957_MAX_HISTO_SAMPLES* sizeof( *histo_buff));
						if( histo_buff== NULL)
						{
							TRACE("erro_8\n");
							goto exit_point;
						}
						memset( histo_buff, 0, N957_MAX_HISTO_SAMPLES* sizeof( *histo_buff));
						
					}					
					paused= !paused;
					
					break;
				case 'a': // 8192 canais
					fator = 1;
					break;

				case 'b':// 4096 canais
					fator = 2;
					break;
				case 'c': // 2048 canais
					fator = 4;
					break;
				case 'd': // 1024 canais
					fator = 8;
					break;
				case 'e': // 512 canais
					fator = 16;
					break;
				case 'f': // 256 canais
					fator = 32;
					break;
				case 'g':// 128 canais
					fator = 64;
					break;
				case 'h': // 64 canais
					fator = 128;
					break;
				case 'i': // função que transmite a informação para o python
					TRACE("OK\n");
					TRACE("[");
					for(w=0; w<(8192-fator+1); w=w+fator) 
						{
					 	for(ii=w; ii<=(w+fator-1); ii++)
							temp = temp + histograma[ii];
						TRACE1("%i",temp);
						if (w!=(8192-fator)) 
							TRACE(",");
						temp = 0;
						//JULIO w=w+fator;
						}
					TRACE("]\n");
					N957_GetScaler(user_setting.m_N957_handle, &data32);
					TRACE1("%d\n",data32 + nmr_ADC_pause);
					N957_GetTimer(user_setting.m_N957_handle, &data32);
					TRACE1("%d\n",data32 +tempo_pause);
					N957_GetLiveTime(user_setting.m_N957_handle, &data32);
					TRACE1("%d\n",data32 +tempo_pause_vivo);

					/*N957_UINT16 tamanho;
					N957_GetBufferOccupancy( user_setting.m_N957_handle, &tamanho);

					TRACE1("%d\n",tamanho);*/  // mostra se quntas contagens tem no buffer
					break;
				case 'j': // funcao testada
					lldt= ((getchar())*10 + getchar());
					N957_SetLLD( user_setting.m_N957_handle, lldt);

					break;
				case 'l':
					goto exit_point;
					//N957_SetLLD( user_setting.m_N957_handle, lldt);
					//lldt=0;
					break;
				case 'z': // salva o arquivo  em "save.dat" o qual vai ser renomeado pelo programa em python
					log = fopen("save.dat", "w+");
					for(w=0; w<8192; w++)
						fprintf(log, "%i\n", histograma[w]);
					fclose(log);
					break;
				case 'y': // Abre o arquivo com o nome "save.dat"
					log = fopen("save.dat", "r");
					for(w=0; w<(8192-fator+1); w=w+fator)
						{
						fscanf(log, "%i", &temp);
					 	for(i=w; i<=(w+fator-1); i++)
							histograma[i] = temp/fator;
						}
					fclose(log);
					break;
                                case 'k':
					TRACE("OK\n");

					N957_GetConfigROM( user_setting.m_N957_handle, &ROM);

					TRACE1("%u \n" ,ROM.m_serial);					

					break;
                                case'm': // Set Auto Gate      
                                        N957_SetAcquisitionMode(user_setting.m_N957_handle, N957ControlModeAuto);
                                        GATE=TRUE;
                                        break;
                                case'n':// Set external Gate|
                                        N957_SetAcquisitionMode(user_setting.m_N957_handle, N957ControlModeExtGate);
                                        GATE=FALSE;
                                        break;                                     

				}
			}
			//TRACE("flag: 3 \n");
		for( i= 0; i< data_read; i++) {			
			// Calculate the histogram
			++histo_buff[ data_buff[ i]& ( N957_MAX_HISTO_SAMPLES- 1)];
			// Log to file (if enabled)
			sprintf( aux_string, "%04d\n", data_buff[ i]);
			if (!paused)
				histograma[atoi(aux_string)+1] = histograma[atoi(aux_string)+1] +1;
			//TRACE("flag: 4 \n");

		}
	}

	if( user_setting.m_debug)
	{
		//
		// clear software convertion
		if( N957_SetSwConvFlag( user_setting.m_N957_handle, N957_FALSE))
		{
			TRACE("erro_20\n");
			goto exit_point;
		}
	}

exit_point:

	

	// Stop acquisition
	if(N957_StopAcquire( user_setting.m_N957_handle))
	{
		TRACE("erro_24\n");
		//goto exit_point;
	}

	if(N957_SwClear(user_setting.m_N957_handle))
	{
		TRACE("erro_25\n");
		//goto exit_point;
	}	

	// close modules
	user_settings_close( &user_setting);

	return ret_val;
}

